﻿/*

    ZyConf - Konfigurator bezpecnostnich bran ZyXEL

    Class MainWindow
    Slouzi k bezpecnosti aplikace
        
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplicationZyConf
{
    public partial class MainWindow : Window
    {
        public static int authorized = 0;
        public static string passFromFile = "";
        public MainWindow()
        {
            InitializeComponent();
        }

        /*
         * Window_Loaded 
         * Obsluzna funkce pri nacteni okna
         */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            passwd.Focus();

            //nacteni pripojovacich udaju z konfiguracnich souboru
            try
            {
                //overeni, ze jsou prilozeny knihovny - dulezite pro SSH 
                if (!File.Exists(WindowContent.path + WindowContent.diffieDll) || !File.Exists(WindowContent.path + WindowContent.schedDll) || !File.Exists(WindowContent.path + WindowContent.secDll) || !File.Exists(WindowContent.path + WindowContent.sharpSSHDll))
                {
                    MessageBox.Show("Knihovny byly smazány, program nemůže být načten! \nERROR: .DLL IS MISSING", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);
                    DateTime datumCas = DateTime.Now;

                    Environment.Exit(0); //pokud nejsou potrebne soubory dostupne - aplikace nemuze fungovat - vypnuti
                }               

                getText.readFile(WindowContent.configTXT); //pokud je vse OK -> nacist konfiguracni soubor
                passFromFile = cipher.DecryptStr(getText.linesFromFile[6], cipher.myRijndaelKey, cipher.myRijndaelIV); //nacteni spravneho hesla

            }
            catch
            {
                MessageBox.Show("Poškozené konfigurační soubory při přihlášení!", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                Util.saveToLog("Neplatný pokus o spuštění programu", "Poškozena integrita dat, vadné konfigurační soubory.");

                Environment.Exit(0);
            }      
        }

        /*
        * processPswd_Click 
        * Obsluzna funkce pri vstupu do aplikace
        */
        private void processPswd_Click(object sender, RoutedEventArgs e)
        {
            Authorization(passwd);
        }

        /*
        * passwd_GotMouseCapture_1 
        * Obsluha pri kliknuti do pole s heslem
        */
        private void passwd_GotMouseCapture_1(object sender, MouseEventArgs e)
        {
            passwd.SelectAll();
        }

        /*
        * passwd_GotFocus_1 
        * Obsluha pri kliknuti do pole s heslem
        */
        private void passwd_GotFocus_1(object sender, RoutedEventArgs e)
        {
            passwd.SelectAll();
        }

        /*
        * passwd_KeyDown 
        * Reakce na enter
        */
        private void passwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Authorization(passwd);
            }
        }

        /*
        * Authorization 
        * Overeni hesla
        */
        public void Authorization(PasswordBox passwd)
        {
            if (passwd.Password == passFromFile)
            {
                MainWindow.authorized = 1; //kontrola pro dalsi okna
                WindowContent window = new WindowContent();

                Util.saveToLog("Přihlášení", "");

                window.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Nesprávné heslo!");

                Util.saveToLog("Zadání neplatného hesla (" + passwd.Password + ")", "");
            }
        }      
    }
}
