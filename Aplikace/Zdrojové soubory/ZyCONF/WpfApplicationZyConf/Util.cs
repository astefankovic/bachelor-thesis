﻿/*

    ZyConf - Konfigurator bezpecnostnich bran ZyXEL

    Class Util
    Slouzi ke komunikaci pomoci SSH
        
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections;
using Tamir.SharpSsh;
using System.Windows;
using System.Threading;
using System.Windows.Media;
using System.Net.NetworkInformation;

namespace WpfApplicationZyConf
{
    public class Util
    {
        public static string commandReturn = ""; //globalni promenna pro ulozeni vystupu
        public static int countOfstandard = 1; //slouzi pro cteni z konzole
        public static Statement connectStatement = new Statement(); //info o pripojeni

        /*
         * IsAvaible 
         * funkce overi dostupnost dane ip adresy
         * @param host ip adresa bezpecnostni brany
         */
        public static bool IsAvaible(string host)
        {
            try
            {
                Ping p = new Ping();
                PingReply r;
                string ip = host;
                r = p.Send(ip);

                if (r.Status == IPStatus.Success)
                    return true;
                else
                    return false;
            }

            catch
            {
                MessageBox.Show("Není možné pingovat IP adresu " + host, "Ping", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
        }

        /*
         * SshConnect 
         * Pripojeni k serveru
         */
        public static SshShell SshConnect(string host, string user, string pswd)
        {
            commandReturn = "";      
            SshConnectionInfo input = Util.GetInput(host, user, pswd);
            SshShell ssh = new SshShell(input.Host, input.User);
            if (input.Pass != null) 
                ssh.Password = input.Pass;


            //MessageBox.Show("host:" + host + " user:" + user + " passwd:" + pswd + ".");
              
            try
            {
                ssh.Connect(); //pokus o pripojeni
                
                connectStatement.statementResult = "Připojeno";
                connectStatement.color = (Color)ColorConverter.ConvertFromString("#FF46FF00"); //zelená
                commandReturn = ("SSH spojení bylo navázáno.");
            }
            catch //pokud se nelze pripojit
            {
                connectStatement.statementResult = "SSH spojení se nepodařilo. ERROR(SshConnect catch)";
                connectStatement.color = (Color)ColorConverter.ConvertFromString("#FFFF0000"); //červená
                commandReturn = ("SSH připojení nemohlo být navázáno. Zkuste jiné přihlašovací údaje a ověřte, že se skutečně jedná o kompatibilní zařízení ZyXEL.");
            }

            return ssh; //vrati SshShell (CMD) - moznost do ni zapisovat a cist
        }

        /*
         * SshDisconnect 
         * Odpojeni od serveru
         */
        public static void SshDisconnect(SshShell ssh)
        {            
            try
            {
                ssh.Close(); //pokus o ukonceni spojeni
                connectStatement.statementResult = "Spojení bylo ukončeno";
                connectStatement.color = (Color)ColorConverter.ConvertFromString("#FFFF8B00"); //oranžová
            }
            catch //pokud jiz doslo k necekanemu odpojeni
            {
                connectStatement.statementResult = "Spojení nemohlo být řádně ukončeno, zařízení je již pravděpodobně nedostupné. ERROR(SshDisconnect catch)";
                connectStatement.color = (Color)ColorConverter.ConvertFromString("#FFFF0000"); //červená                
            }
        }

        /*
         * doCommand 
         * Provedeni prikazu
         */
        public static void doCommand(SshShell ssh, Boolean getMessage, Boolean configMode, String command, Boolean needWrite)
        {
            commandReturn = "";
            
            if (ssh.ShellOpened) //pokud bylo SSH navazno
            {
                string pattern;  //očekávaný znak příkazové řádky (standartně #, >)            

                try
                {
                    //zajištění přechodu do konfiguračního módu
                    if (configMode == true) //pokud je pro vykonani prikazu nutny konfiguracni mod FW
                    {
                        pattern = "#";
                        ssh.WriteLine("configure terminal");
                    }
                    else
                        pattern = ">";                  
                    
                    if(countOfstandard == 0)
                        ssh.Expect(pattern);

                    ssh.ExpectPattern = pattern;
                    ssh.RemoveTerminalEmulationCharacters = true;
                    
                    //provedení příkazu
                    ssh.WriteLine(command);


                    if (getMessage == true && configMode == true)//pokud je potřeba zajištění uložení zprávy od serveru - v konfiguracnim modu
                    {
                        Thread.Sleep(500); //kvůli komunikaci se musí chvíli počkat
                        commandReturn = ssh.Expect(pattern);//ulozeni vracene hodnoty
                    }
                    else if (getMessage == true && configMode != true)//pokud je potřeba zajištění uložení zprávy od serveru - v enabled modu
                    {                        
                        Thread.Sleep(500);  
                        commandReturn = ssh.Expect(pattern);                        
                    }
                    else if (getMessage == false && configMode == true)//pokud neni nutne ulozit zpravu - v konfiguracnim modu
                    {                       
                        Thread.Sleep(500);      
                        commandReturn = "Příkaz byl úspěšně proveden.";
                    }
                    else if (getMessage == false && configMode == false)//pokud neni nutne ulozit zpravu - v enabled modu
                    {                        
                        Thread.Sleep(500); 
                        commandReturn = "Příkaz byl úspěšně proveden.";
                    }

                    if(needWrite == true)
                           ssh.WriteLine("write"); //je nutne ulozit konfiguraci do bezpecnotni brany
                }
                catch
                {
                    commandReturn = "Nastala chyba v příkazu, je možné že zařízení nepodporuje daný příkaz. ERROR(doCommand catch)";
                }                
            }
            else
            {
                commandReturn = ("SSH příkaz se nepodařilo provést. Zkuste jiné přihlašovací údaje a ověřte, že se skutečně jedná o kompatibilní zařízení ZyXEL.");                
            }
        
        }

        /*
         * GetInput 
         * Slouzi pro komunikaci se serverem - init data
         */
        public static SshConnectionInfo GetInput(string host, string user, string pswd)
        {
            SshConnectionInfo info = new SshConnectionInfo();           

            info.Host = host;
            info.User = user;
            info.Pass = pswd;

            return info;           
        }

        /*
      * saveToLog
      * Ulozi do log.txt
      */
        public static void saveToLog(String description, String detail)
        {
            //zapis do logu

            try
            {
                DateTime datumCas = DateTime.Now;
                System.IO.StreamWriter file = new System.IO.StreamWriter(WindowContent.path + WindowContent.logTXT, true);
                file.WriteLine(System.Environment.NewLine + "{0}", datumCas);
                file.WriteLine("\n\t-----> " + description);
                file.Write("\n\t\t " + detail);

                file.Close();
            }
            catch
            {
                MessageBox.Show("Nepodařilo se zapsat do logu, kontaktuje prosím správce sítě. Můžete pokračovat ve své činnosti.", "Přístup k logu byl odepřen", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }

        }
    }

    /*
     * SshConnectionInfo 
     * Struktura uklada init data pro pripojeni
     */
    public struct SshConnectionInfo
    {
        public string Host;
        public string User;
        public string Pass;
        public string IdentityFile;
    }

    /*
    * SshConnectionInfo 
    * Infomace o stavovem radku
    */
    public struct Statement
    {
        public string statementResult;
        public Color color;
    }

    
}
