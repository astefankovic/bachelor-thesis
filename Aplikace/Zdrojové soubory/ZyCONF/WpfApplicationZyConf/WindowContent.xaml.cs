﻿/*

    ZyConf - Konfigurator bezpecnostnich bran ZyXEL

    Class WindowContent
    Slouzi ke konfiguraci a rizeni UI aplikace
        
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tamir.SharpSsh;
using Microsoft.Win32.TaskScheduler;
using System.Security.Cryptography;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;


namespace WpfApplicationZyConf
{
    public partial class WindowContent : Window
    {
        public WindowContent()
        {
            InitializeComponent();
        }

        //inicializace promennych
        public static string host = ""; //ip adresa bezpecnostni brany
        public static string user = ""; //username
        public static string pswd = ""; //user - passwd
        public static string identifikator = ""; //jednoznacne ID bezpecnostni brany
        public static string useremail = "";
        public static string hour = "";
        public static string appPass = "";
        public SshShell ssh;
        public DaysOfTheWeek day;  
        public static string path = System.IO.Directory.GetCurrentDirectory();
        public static string configTXT = @"\config\connection.txt";
        public static string configTXTexample = @"\config\connection_example.txt";
        public static string uiTXTexample = @"\config\ui_example.txt";
        public static string ipaddressesTXT = @"\config\ipaddress.txt";
        public static string srvEXE = @"\ZyREBOOTnow.exe";
        public static string hashPath = System.IO.Path.GetPathRoot(path) + "//tmp//hcheck.txt";
        public static string logTXT = @"\config\log.txt";
        public static string diffieDll = @"\DiffieHellman.dll";
        public static string schedDll = @"\Microsoft.Win32.TaskScheduler.dll";
        public static string secDll = @"\Org.Mentalis.Security.dll";
        public static string sharpSSHDll = @"\Tamir.SharpSSH.dll";
        public static string fileForWindow = "";

        /*
         * Window_Loaded 
         * definice cinnosti po startu aplikace
         */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           //nacteni pripojovacich udaju z konfiguracnich souboru
           try
           {
               if (!File.Exists(path + diffieDll) || !File.Exists(path + schedDll) || !File.Exists(path + secDll) || !File.Exists(path + sharpSSHDll))
               {
                   MessageBox.Show("Knihovny byly smazány, program nemůže být načten! \nERROR: .DLL IS MISSING", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                   Util.saveToLog("Neplatný pokus o spuštění programu", "Neexistující knihovny.");

                   Environment.Exit(0);
               }
               //DaysOfTheWeek day;

               //DaysOfTheWeek day; 
               

               using (TaskService ts = new TaskService())
               {
                   if(ts.FindTask("ZyXEL_rebootTask", true) != null)
                   {
                       TaskDefinition td = ts.FindTask("ZyXEL_rebootTask", true).Definition;
                       Microsoft.Win32.TaskScheduler.TriggerCollection trigger;//= new Microsoft.Win32.TaskScheduler.TriggerCollection();
                       trigger = td.Triggers;
                       string a = trigger.ToString();
                       string[] b = a.Split(' ');
                       hour = b[1];

                       setDayToTB(b[3]);
                   }
                   else
                   {
                       hour = "0:00";
                       day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Sunday;
                       Days_ComboBox.Text = "Neděle";

                   }
               }

                string configFileHash = cipher.HashSHA1(path + configTXT);
                getText.readFile("\\hcheck.txt");

                if (configFileHash != getText.linesFromFile[0])
                {
                    MessageBox.Show("Poškozena integrita dat! Konfigurační soubory byly změněny, program nemůže být načten!", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                    Util.saveToLog("Neplatný pokus o spuštění programu", "Poškozena integrita dat.");

                    Environment.Exit(0);
                }                  

                getText.readFile(configTXT);

                //ulozeni do golbalnich promennych
                serverIP_TextBox.Text = cipher.DecryptStr(getText.linesFromFile[0], cipher.myRijndaelKey, cipher.myRijndaelIV);
                userName_TextBox.Text = cipher.DecryptStr(getText.linesFromFile[1], cipher.myRijndaelKey, cipher.myRijndaelIV); 
                userPassword_TextBox.Password = cipher.DecryptStr(getText.linesFromFile[2], cipher.myRijndaelKey, cipher.myRijndaelIV);
                IDName_TextBox.Text = cipher.DecryptStr(getText.linesFromFile[3], cipher.myRijndaelKey, cipher.myRijndaelIV); 
                toEmail_TextBox.Text = cipher.DecryptStr(getText.linesFromFile[4], cipher.myRijndaelKey, cipher.myRijndaelIV);
                pass_TextBox.Password = cipher.DecryptStr(getText.linesFromFile[6], cipher.myRijndaelKey, cipher.myRijndaelIV);
                time_TextBox.Text = hour;

            }
            catch
            {
                MessageBox.Show("Poškozené konfigurační soubory!", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                Util.saveToLog("Neplatný pokus o spuštění programu", "Poškozena integrita dat, vadné konfigurační soubory.");
                Environment.Exit(0);
            }            
        }

        /*
         * setConnection 
         * ulozeni dat od uzivatele do promennych
         */
        private int setConnection()
        {            
            host = serverIP_TextBox.Text;
            user = userName_TextBox.Text;
            pswd = userPassword_TextBox.Password;
            String tempday = ((ComboBoxItem)Days_ComboBox.SelectedItem).Content.ToString();
            hour = time_TextBox.Text;
            int result = 0;

            if (host == "" || user == "" || pswd == "" || tempday == "" || hour == "")
            {
                result += 1;
            }

            switch (tempday)
            {
                case "Pondělí":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Monday;
                    break;
                case "Úterý":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Tuesday;
                    break;
                case "Středa":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Wednesday;
                    break;
                case "Čtvrtek":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Thursday;
                    break;
                case "Pátek":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Friday;
                    break;
                case "Sobota":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Saturday;
                    break;
                case "Neděle":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Sunday;
                    break;
                default:
                    break;
            }

            return result;
        }

        /*
         *setDeyToTB
         *nastaví den to rolovaciho policka 
         *string dayFromSched - den vyparsovany ze scheduleru
         */
        public void setDayToTB(string dayFromSched)
        {
            switch (dayFromSched)
            {
                case "pondělí":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Monday;
                    Days_ComboBox.Text = "Pondělí";
                    break;
                case "úterý":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Tuesday;
                    Days_ComboBox.Text = "Úterý";
                    break;
                case "středa":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Wednesday;
                    Days_ComboBox.Text = "Středa";
                    break;
                case "čtvrtek":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Thursday;
                    Days_ComboBox.Text = "Čtvrtek";
                    break;
                case "pátek":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Friday;
                    Days_ComboBox.Text = "Pátek";
                    break;
                case "sobota":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Saturday;
                    Days_ComboBox.Text = "Sobota";
                    break;
                case "neděle":
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Sunday;
                    Days_ComboBox.Text = "Neděle";
                    break;
                default:
                    day = Microsoft.Win32.TaskScheduler.DaysOfTheWeek.Friday;
                    Days_ComboBox.Text = "Neděle";
                    break;
            }

        }

        /*
         * update_Statement 
         * zmena barvy textu
         */
        private void update_Statement() 
        {
            connectionStatus_TextBlock.Foreground = new System.Windows.Media.SolidColorBrush(Util.connectStatement.color);
            connectionStatus_TextBlock.Text = Util.connectStatement.statementResult;
        }

        /*
        * deviceStatement 
        * nacteni informaci o zarizeni
        */
        private String deviceStatement()
        {
            if (setConnection() == 0)
            {
                if (Util.IsAvaible(host))
                {
                    String status = "Zařízení je dosažitelné.";
                    String value;
                    Util.commandReturn = "";

                    //MessageBox.Show(host + "." + user + "." + pswd);

                    ssh = Util.SshConnect(host, user, pswd);
                    update_Statement();
                    status += "\n" + Util.commandReturn;

                   // MessageBox.Show(Util.commandReturn);
                   
                    //ověření jestli se podařilo připojit - určení kde přesně je chyba   
                    if (Util.commandReturn == "SSH spojení bylo navázáno.")
                    {
                        Util.doCommand(ssh, true, false, "show version", false); //prikaz CMD ZyXELu
                            value = getText.between(Util.commandReturn, ">", "Router");
                            status += "\n---------------------------------------\nInformace o firmwaru:" + value;

                        Util.doCommand(ssh, true, false, "show serial-number", false); //prikaz CMD ZyXELu
                            value = getText.between(Util.commandReturn, "serial-number", "Router");
                            status += "\n" + value + "\n---------------------------------------\n";

                       Util.doCommand(ssh, true, false, "show led status", false); //prikaz CMD ZyXELu
                            if (Util.commandReturn.Contains("green"))
                                status += ("\n\nKontrolka svítí zeleně.");
                            else if (Util.commandReturn.Contains("red"))
                                status += ("\n\nKontrolka svítí ČERVENĚ.");
                            else
                                status += ("\n\nKontrolka SE NECHOVÁ PŘIROZENĚ.");

                       Util.doCommand(ssh, true, false, "show mem status", false); //prikaz CMD ZyXELu
                            value = getText.between(Util.commandReturn, ":", "%");
                            status += "\nPaměť je vytížena na" + value + "%.\n";

                        Util.SshDisconnect(ssh);
                    }
                    else
                        return status;                                  

                    return status;
                }
                else
                    return "IP adresa není dosažitelná.";
            } 
            else
                return "Vyplňte všechna potřebná pole.";
        }


        /*
         * Restartovat
        */ 
        private void reset_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(path + srvEXE);
            }
            catch
            {
                MessageBox.Show("Soubor ZyREBOOTnow.exe neexistuje", "Chybějící soubor", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
            
        }

        /*
         * Zjistit dostupnost
        */ 
        private void avaibility_Button_Click(object sender, RoutedEventArgs e)
        {
            statusWindow statusWindow = new statusWindow();
            statusWindow.richTextBoxText = deviceStatement();
            
            statusWindow.Show();

            update_Statement();
        }

        /*
         * Okno pro definici IP adres
         */ 
        private void setIP_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                setIPwindow setIPwindow = new setIPwindow();
                fileForWindow = path + ipaddressesTXT;
                string text = "";

                text = System.IO.File.ReadAllText(path + ipaddressesTXT);

                setIPwindow.richTextBoxIPText = text;
                setIPwindow.Show();
                update_Statement();
            }
            catch //pokud soubor neexistuje - vytvori se novy - s popisem radku
            {
                try
                {
                    String path = System.IO.Directory.GetCurrentDirectory();
                    String textToFile = "192.168.1.1" + Environment.NewLine + "192.168.1.52" + Environment.NewLine + "google.com"; //preddefinovany text
                    System.IO.File.WriteAllText(path + ipaddressesTXT, textToFile);

                    setIPwindow setIPwindow = new setIPwindow();
                    fileForWindow = ipaddressesTXT;//nastaveni pro definici okna setIP
                    string text = "";

                    try
                    {
                        text = System.IO.File.ReadAllText(path + configTXTexample);
                    }
                    catch
                    {
                        text = "";
                    }

                    setIPwindow.richTextBoxIPText = text;
                    setIPwindow.Show();
                    update_Statement();

                    //System.Diagnostics.Process.Start(path + @"\config\connection_example.txt");
                }
                catch
                {
                    MessageBox.Show(@"Je nám líto, ale soubor " + ipaddressesTXT + " nemohl být načten.", "Chybějící soubor s nápovědou", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
        }

        /*
         * Nastaveni windows scheduleru
         */ 
        private void paramReset_Button_Click(object sender, RoutedEventArgs e)
        {
            if (setConnection() == 0)
            {

                string text = System.IO.File.ReadAllText(path + configTXT);

                if(hour.Length != 5 || !hour.Contains(':'))
                {
                    MessageBox.Show("Čas musí být ve formátu HH:mm", "Špatný vstup", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                using (TaskService ts = new TaskService())
                {
                    TaskDefinition td = ts.NewTask();
                    td.RegistrationInfo.Description = "Start zyrebootnow.exe for repeatly reboot ZyXEL device.";
                    td.Principal.UserId = "SYSTEM";

                    DateTime today = DateTime.Today;
                    today = today.AddDays(-1);

                    WeeklyTrigger week = new WeeklyTrigger();
                    try
                    {
                        week.StartBoundary = Convert.ToDateTime(today.ToShortDateString() + " " + hour + ":00");
                    }
                    catch
                    {
                        MessageBox.Show("Čas musí být ve formátu HH:mm", "Špatný vstup", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                    

                    week.WeeksInterval = 1;
                    week.DaysOfWeek = day;
                    td.Triggers.Add(week);

                    try 
                    {
                        td.Actions.Add(new ExecAction("\"" + path + @"\ZyREBOOTnow.exe" + "\"", null, path));
                        ts.RootFolder.RegisterTaskDefinition("ZyXEL_rebootTask", td);
                    }
                    catch
                    {
                        td.Principal.UserId = null;

                        td.Actions.Add(new ExecAction("\"" + path + @"\ZyREBOOTnow.exe" + "\"", null, path));
                        ts.RootFolder.RegisterTaskDefinition("ZyXEL_rebootTask", td);
                        MessageBox.Show("Aplikace nebyla spuštěna s dostatečnými oprávněními. \nPo odhlášení nemusí opakovaný restart proběhnout. \nZkontrolujte nastavení v scheduleru.", "Chybí potřebná oprávnění", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                    }

                    
                }

                MessageBox.Show("Událost byla nastavena.");
            }
            else
                MessageBox.Show("Vyplňte všechna potřebná pole.", "Chybí potřebná pole", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        /*
         * Ulozeni prihlasovacich udaju - sifrovane + generovani hash souboru
         */ 
        private void save_Button_Click(object sender, RoutedEventArgs e)
        {
            Encoding czech = Encoding.GetEncoding(1250); //dulezite pro diakritiku
            System.IO.StreamWriter file = new System.IO.StreamWriter(path + configTXT, false, czech);
            
            file.WriteLine(cipher.EncryptStr(serverIP_TextBox.Text, cipher.myRijndaelKey, cipher.myRijndaelIV));
            file.WriteLine(cipher.EncryptStr(userName_TextBox.Text, cipher.myRijndaelKey, cipher.myRijndaelIV));
            file.WriteLine(cipher.EncryptStr(userPassword_TextBox.Password, cipher.myRijndaelKey, cipher.myRijndaelIV));
            file.WriteLine(cipher.EncryptStr(IDName_TextBox.Text, cipher.myRijndaelKey, cipher.myRijndaelIV));
            file.WriteLine(cipher.EncryptStr(toEmail_TextBox.Text, cipher.myRijndaelKey, cipher.myRijndaelIV));
            file.WriteLine(cipher.EncryptStr(room_TextBox.Text, cipher.myRijndaelKey, cipher.myRijndaelIV));
            file.WriteLine(cipher.EncryptStr(pass_TextBox.Password, cipher.myRijndaelKey, cipher.myRijndaelIV));
            file.Close();

            System.IO.StreamWriter hashFile = new System.IO.StreamWriter(path + "\\hcheck.txt");
            hashFile.WriteLine(cipher.HashSHA1(path + configTXT));
            hashFile.Close();

            MessageBox.Show("Nastavení bylo uloženo.");
        }

        /*
         * Otevreni windows scheduleru
         */ 
        private void openScheduler_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Taskschd.msc");
            }
            catch
            {
                MessageBox.Show("Nepodařilo se otevřít Plánovač úloh.", "Task scheduler", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
           
        }

        /*
         * Otevreni pruzkumnika souboru pro zasifrovani
         */ 
        private void cipherIT_Button_Click(object sender, RoutedEventArgs e)
        {
            // vytvori dialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            string filename = "";

            // filter na txt
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt";

            Nullable<bool> result = dlg.ShowDialog();

            //overeni ze bylo neco vybrano
            if (result == true)
            {
                // otevre dokument
                filename = dlg.FileName;
            }
            else
                return;

            Encoding czech = Encoding.GetEncoding(1250);
            List<string> lines = new List<string>();

            using (StreamReader r = new StreamReader(filename, Encoding.UTF8))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            
            //nacteni dat o puvodnim souboru
            string extension = System.IO.Path.GetExtension(filename);
            string filenameOnly = System.IO.Path.GetFileName(filename);
            string filenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(filename);
            string directory = System.IO.Path.GetDirectoryName(filename);
            int check = 0;

            //vytvoreni zasifrovaneho souboru
            System.IO.StreamWriter file = new System.IO.StreamWriter(directory + "\\" + filenameNoExtension + "_encrypted" + extension, false, czech);

            foreach (string s in lines)
            {
                if (cipher.EncryptStr(s, cipher.myRijndaelKey, cipher.myRijndaelIV) != null)
                    file.WriteLine(cipher.EncryptStr(s, cipher.myRijndaelKey, cipher.myRijndaelIV));
            }
            file.Close();

            //vytvoreni kontrolniho souboru
            System.IO.StreamWriter hashFile = new System.IO.StreamWriter(directory + "\\" + filenameNoExtension + "_encrypted_hcheck" + extension, false, czech);
            hashFile.WriteLine(cipher.HashSHA1(directory + "\\" + filenameNoExtension + "_encrypted" + extension));
            hashFile.Close();

            if (check == 0)
                if (MessageBox.Show("Byl vytvořen zašifrovaný soubor \n\t" + filenameNoExtension + "_encrypted" + extension + "\n a kontrolní soubor \n\t" + filenameNoExtension + "_encrypted_hcheck" + extension +  "\nChcete otevřít složku s jejich umístěním?", "Zašifrovaný soubor", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Process.Start(directory);
                }
        }

        /*
         * Otevreni pruzkumnika souboru pro desifrovani
         */ 
        private void decipherIT_Button_Click(object sender, RoutedEventArgs e)
        {
            // Otevre pruzkumnika
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            string filename = "";

            // Filter pro priponu souboru
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt";

            Nullable<bool> result = dlg.ShowDialog();

            // Zajisti jestli bylo neco vybrano
            if (result == true)
            {
                // otevre dokument
                filename = dlg.FileName;
            }
            else
                return;
            
            Encoding czech = Encoding.GetEncoding(1250);
            List<string> lines = new List<string>();//radky ze souboru

            using (StreamReader r = new StreamReader(filename, czech))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }

            //nacteni dat o puvodnim souboru
            string extension = System.IO.Path.GetExtension(filename);
            string filenameOnly = System.IO.Path.GetFileName(filename);
            string filenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(filename);
            string directory = System.IO.Path.GetDirectoryName(filename);


            //vytvoreni desifrovaneho souboru
            System.IO.StreamWriter file = new System.IO.StreamWriter(directory + "\\" + filenameNoExtension + "_decrypted" + extension);

            foreach (string s in lines)
            {
               file.WriteLine(cipher.DecryptStr(s, cipher.myRijndaelKey, cipher.myRijndaelIV));            
            }
            file.Close();

            if(cipher.integrityCheck == 0)
            {
                File.Delete(directory + "\\" + filenameNoExtension + "_decrypted" + extension);

            }
            else
                if (MessageBox.Show("Byl vytvořen dešifrovaný soubor \n\t" + filenameNoExtension + "_decrypted" + extension + "\n a kontrolní soubor \n\t" + filenameNoExtension + "_decrypted_hcheck" + extension + "\nChcete dešifrovaný soubor editovat?", "Zašifrovaný soubor", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        setIPwindow setIPwindow = new setIPwindow();
                        fileForWindow = directory + "\\" + filenameNoExtension + "_decrypted" + extension;
                        string text = "";

                        text = System.IO.File.ReadAllText(directory + "\\" + filenameNoExtension + "_decrypted" + extension);

                        setIPwindow.richTextBoxIPText = text;
                        setIPwindow.Show();
                        update_Statement();
                    }
                    catch
                    {
                        MessageBox.Show("Nepodařilo se otevřít soubor " + filenameNoExtension + "_decrypted" + extension, "Chyba otevření", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }

            cipher.integrityCheck = 1;

        }

        /*
         * Otevreni souboru s napovedu
         */ 
        private void help_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(path + @"\config\help.pdf");//nacteni user guide
            }
            catch
            {
                MessageBox.Show(@"Je nám líto, ale soubor s nápovědou \config\help.pdf nemohl být načten.", "Chybějící soubor s nápovědou", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }

        /*
         * Otevreni okna s konfig. souborem
         */ 
        private void openInit_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {              
                setIPwindow setIPwindow = new setIPwindow();
                fileForWindow = path + configTXTexample;
                string text = "";
               
                    text = System.IO.File.ReadAllText(path + configTXTexample);

                setIPwindow.richTextBoxIPText = text;
                setIPwindow.Show();
                update_Statement();
            }
            catch //pokud soubor neexistuje - vytvori se novy - s popisem radku
            {
                try
                {
                    String path = System.IO.Directory.GetCurrentDirectory();
                    String textToFile = "192.168.1.1" + Environment.NewLine + "UserForRebootSSh" + Environment.NewLine + "PasswordForUser" + Environment.NewLine + "ID of firewall machine" + Environment.NewLine + "email@email.com" + Environment.NewLine + "RoomNumber" + Environment.NewLine + "Pa$$w00rdForApplication"; //preddefinovany text
                    System.IO.File.WriteAllText(path + configTXTexample, textToFile);

                    setIPwindow setIPwindow = new setIPwindow();
                    fileForWindow = path + configTXTexample;
                    string text = "";

                    try
                    {
                        text = System.IO.File.ReadAllText(path + configTXTexample);
                    }
                    catch
                    {
                        text = "";
                    }


                    setIPwindow.richTextBoxIPText = text;
                    setIPwindow.Show();
                    update_Statement();

                    //System.Diagnostics.Process.Start(path + @"\config\connection_example.txt");
                }
                catch
                {
                    MessageBox.Show(@"Je nám líto, ale soubor " + configTXTexample + " nemohl být načten.", "Chybějící soubor s nápovědou", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
        }

        /*
         * Otevreni okna s konfig. souborem
         */ 
        private void openInitUI_Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                setIPwindow setIPwindow = new setIPwindow();
                fileForWindow = path + uiTXTexample;
                string text = "";


                text = System.IO.File.ReadAllText(path + uiTXTexample);



                setIPwindow.richTextBoxIPText = text;
                setIPwindow.Show();
                update_Statement();
            }
            catch //pokud soubor neexistuje - vytvori se novy - s popisem radku
            {
                try
                {
                    String path = System.IO.Directory.GetCurrentDirectory();
                    String textToFile = "#Název politiky" + Environment.NewLine + "@popis co vše toto nastavení zakáže - tooltip" + Environment.NewLine + "&F5-2-3"; //preddefinovany text
                    System.IO.File.WriteAllText(path + uiTXTexample, textToFile);

                    setIPwindow setIPwindow = new setIPwindow();
                    fileForWindow = path + uiTXTexample;
                    string text = "";

                    try
                    {
                        text = System.IO.File.ReadAllText(path + uiTXTexample);
                    }
                    catch
                    {
                        text = "";
                    }


                    setIPwindow.richTextBoxIPText = text;
                    setIPwindow.Show();
                    update_Statement();

                }
                catch
                {
                    MessageBox.Show(@"Je nám líto, ale soubor " + uiTXTexample + " nemohl být načten.", "Chybějící soubor s nápovědou", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
        } 

        /*
         *  dale jen akce s prvky programu - user-friendly - kliknuti do pole = oznaceni textu   
         */
        private void serverIP_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            serverIP_TextBox.SelectAll();
        }

        private void serverIP_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            serverIP_TextBox.SelectAll();
        }

        private void userName_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            userName_TextBox.SelectAll();
        }

        private void userName_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            userName_TextBox.SelectAll();
        }

        private void userPassword_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            userPassword_TextBox.SelectAll();
        }

        private void time_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            time_TextBox.SelectAll();
        }

        private void time_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            time_TextBox.SelectAll();
        }

        private void userPassword_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            userPassword_TextBox.SelectAll();
        }

        private void IDName_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            IDName_TextBox.SelectAll();
        }

        private void IDName_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            IDName_TextBox.SelectAll();
        }

        private void room_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            room_TextBox.SelectAll();
        }

        private void room_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            room_TextBox.SelectAll();
        }

        private void pass_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            pass_TextBox.SelectAll();
        }

        private void pass_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            pass_TextBox.SelectAll();
        } 

        private void toEmail_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            toEmail_TextBox.SelectAll();
        }

        private void toEmail_TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            toEmail_TextBox.SelectAll();
        }

        

        private void userPassword_TextBox_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void userName_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }
        private void serverIP_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }
        private void Window_Activated(object sender, EventArgs e)
        {
        }
        void Window_GotFocus(object sender, RoutedEventArgs e)
        {
        }

                 
    }
}
