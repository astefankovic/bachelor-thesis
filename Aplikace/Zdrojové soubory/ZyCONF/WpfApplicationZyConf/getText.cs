﻿/*

    ZyConf - Konfigurator bezpecnostnich bran ZyXEL

    Class getText
    Slouzi k nacitani radku ze souboru a k jeho parsovani
        
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace WpfApplicationZyConf
{
    class getText
    {
        public static string[] linesFromFile; //pole ve kterem jsou ulozeny radky ze souboru

        /*
         * readFile 
         * načte text do pole - co řádka v .txt to položka v poli linesFromFile
         */
        public static void readFile(String fileName)
        {
            linesFromFile = System.IO.File.ReadAllLines(WindowContent.path + fileName);
        }

        /*
         * between 
         * parsovani textu - obsah mezi dvema retezci
         */
        public static string between(string value, string a, string b)
        {
            int posA = value.IndexOf(a);
            int posB = value.LastIndexOf(b);
            if (posA == -1)
            {
                return "";
            }
            if (posB == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= posB)
            {
                return "";
            }
            return value.Substring(adjustedPosA, posB - adjustedPosA);
        }

        /*
          * after 
          * parsovani textu - obsah za retezcem
          */
        public static string after(string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }

        /*
         * between 
         * parsovani textu - obsah pred retezcem
         */
        public static string Before(string value, string a)
        {
            int posA = value.IndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            return value.Substring(0, posA);
        }

        /*
         * GetRelativePath 
         * zjisti relativni adresu souboru
         */
        public static string GetRelativePath(string filespec, string folder)
        {
            Uri pathUri = new Uri(filespec);
            // Folders must end in a slash
            if (!folder.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()))
            {
                folder += System.IO.Path.DirectorySeparatorChar;
            }
            Uri folderUri = new Uri(folder);
            return @"\" + Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', System.IO.Path.DirectorySeparatorChar));
        }
    }
}
