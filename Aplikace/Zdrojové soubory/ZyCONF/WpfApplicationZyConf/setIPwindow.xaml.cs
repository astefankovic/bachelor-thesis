﻿/*

    ZyConf - Konfigurator bezpecnostnich bran ZyXEL

    Class setIpWindow
    Slouzi ke konfiguraci a rizeni UI aplikace - ulozeni ip adres
        
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplicationZyConf
{
    public partial class setIPwindow : Window
    {
        public static String richTextBoxIPText;

        public setIPwindow()
        {
            InitializeComponent();
        }

        private void richTextBoxIP_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        /*
         * Akce po nacteni okna
         */
        private void Window_Activated(object sender, EventArgs e)
        {
            this.Title = "Úprava souboru " + getText.GetRelativePath(WindowContent.fileForWindow, WindowContent.path);//zmena nadpisu v liste
            String path = System.IO.Directory.GetCurrentDirectory();
            String text = "";
            try
            {
                text = System.IO.File.ReadAllText(WindowContent.fileForWindow);
            }
            catch
            {
                text = "";
            }

            if (getText.GetRelativePath(WindowContent.fileForWindow, WindowContent.path) == WindowContent.ipaddressesTXT)
                cipher_Button.Visibility = System.Windows.Visibility.Hidden;

            richTextBoxIP.Document.Blocks.Clear();
            richTextBoxIP.Document.Blocks.Add(new Paragraph(new Run(text)));
        }

        /*
         * Ulozit
         */
        private void save_Button_Click(object sender, RoutedEventArgs e)
        {
            String path = System.IO.Directory.GetCurrentDirectory();
            String text =  new TextRange(richTextBoxIP.Document.ContentStart, richTextBoxIP.Document.ContentEnd).Text;
            String[] textLines = text.Split('\0');//vsechny radky
            String message = "";

            try
            {
                System.IO.File.WriteAllLines(WindowContent.fileForWindow, textLines);
                message = "Soubor " + WindowContent.fileForWindow + " byl úspěšně uložen.";
            }
            catch
            {
                message = "Soubor " + WindowContent.fileForWindow + " se nepodařilo uložit.";
            }
           

            MessageBox.Show(message, "Save ipaddress.txt", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        /*
         * Nacist
         */
        private void load_Button_Click(object sender, RoutedEventArgs e)
        {
            String path = System.IO.Directory.GetCurrentDirectory();
            String text = "";
            try
            {
                text = System.IO.File.ReadAllText(WindowContent.fileForWindow);
            }
            catch
            {
                text = "";
            }


            richTextBoxIP.Document.Blocks.Clear();
            richTextBoxIP.Document.Blocks.Add(new Paragraph(new Run(text)));
        }

        private void saveCipher_Button_Click(object sender, RoutedEventArgs e)
        {

            String path = System.IO.Directory.GetCurrentDirectory();
            String text = new TextRange(richTextBoxIP.Document.ContentStart, richTextBoxIP.Document.ContentEnd).Text;
            String[] textLines = text.Split('\0');//nactee radky
            String message = "";

            try
            {
                System.IO.File.WriteAllLines(WindowContent.fileForWindow, textLines);
                message = "Soubor " + WindowContent.fileForWindow + " byl úspěšně uložen.";
            }
            catch
            {
                message = "Soubor " + WindowContent.fileForWindow + " se nepodařilo uložit.";
            }
            
            string filename = WindowContent.fileForWindow;
            Encoding czech = Encoding.GetEncoding(1250);
            List<string> lines = new List<string>();

            using (StreamReader r = new StreamReader(filename, Encoding.UTF8))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }

            //nacteni dat o puvodnim souboru
            string extension = System.IO.Path.GetExtension(filename);
            string filenameOnly = System.IO.Path.GetFileName(filename);
            string filenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(filename);
            string directory = System.IO.Path.GetDirectoryName(filename);
            int check = 0;

            //vytvoreni zasifrovaneho souboru
            System.IO.StreamWriter file = new System.IO.StreamWriter(directory + "\\" + filenameNoExtension + "_encrypted" + extension, false, czech);

            foreach (string s in lines)
            {
                if (cipher.EncryptStr(s, cipher.myRijndaelKey, cipher.myRijndaelIV) != null)
                    file.WriteLine(cipher.EncryptStr(s, cipher.myRijndaelKey, cipher.myRijndaelIV));
            }
            file.Close();

            //vytvoreni kontrolniho souboru
            System.IO.StreamWriter hashFile = new System.IO.StreamWriter(directory + "\\" + filenameNoExtension + "_encrypted_hcheck" + extension, false, czech);
            hashFile.WriteLine(cipher.HashSHA1(directory + "\\" + filenameNoExtension + "_encrypted" + extension));
            hashFile.Close();

            if (check == 0)
                if (MessageBox.Show("Byl vytvořen zašifrovaný soubor \n\t" + filenameNoExtension + "_encrypted" + extension + "\n a kontrolní soubor \n\t" + filenameNoExtension + "_encrypted_hcheck" + extension + "\nChcete otevřít složku s jejich umístěním?", "Zašifrovaný soubor", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Process.Start(directory);
                }
        }

        /*
         *openFolder Button
         *otevře složku kde je umisten editovany soubor
         */
        private void openFolder_Button_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(System.IO.Path.GetDirectoryName(WindowContent.fileForWindow));
        }

        /*
         *defaultButton
         * 
         * nacte defaultni hodnoty pro konfiguracni soubory - podle nazvu souboru vybere vhodna data
         */
        private void default_Button_Click(object sender, RoutedEventArgs e)
        {
           String textToTextArea = "Zvolený soubor není určen ani pro jeden z následujících souborů: \n\tipaddress.txt\n\tui.txt\n\tconnection.txt \nNelze vytvořit defultní data.";
            
            if(WindowContent.fileForWindow.Contains("ip"))
                textToTextArea = "192.168.1.1" + Environment.NewLine + "192.168.1.52" + Environment.NewLine + "google.com"; //preddefinovany text

            if (WindowContent.fileForWindow.Contains("ui"))
                textToTextArea = "#Název politiky" + Environment.NewLine + "@popis co vše toto nastavení zakáže - tooltip" + Environment.NewLine + "&F5-2-3"; //preddefinovany text

            if (WindowContent.fileForWindow.Contains("connection"))
                textToTextArea = "192.168.1.1" + Environment.NewLine + "UserForRebootSSh" + Environment.NewLine + "PasswordForUser" + Environment.NewLine + "ID of firewall machine" + Environment.NewLine + "email@email.com" + Environment.NewLine + "RoomNumber" + Environment.NewLine + "Pa$$w00rdForApplication"; //preddefinovany text

            richTextBoxIP.Document.Blocks.Clear();
            richTextBoxIP.Document.Blocks.Add(new Paragraph(new Run(textToTextArea)));  
          
        }
    }
}
