﻿/*

    ZyConf - Konfigurator bezpecnostnich bran ZyXEL

    Class statusWindow
    Slouzi ke konfiguraci a rizeni UI aplikace - zobrazeni statusu bezoecnostni brany
        
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplicationZyConf
{
    public partial class statusWindow : Window
    {
        public statusWindow()
        {
            InitializeComponent();
        }

        public static String richTextBoxText;

        private void RichTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        /*
         * Akce po nacteni okna
         */
        private void Window_Activated(object sender, EventArgs e)
        {
            richTextBox.Document.Blocks.Clear();
            
            richTextBox.Document.Blocks.Add(new Paragraph(new Run(richTextBoxText)));//vlozeni textu
            richTextBox.ScrollToEnd();
        }
    }
}
