﻿/*

    ZyContent - Spoustec pravidel bezpecnostnich bran ZyXEL

    Class CheckBoxAndCommand
    Udrzuje informace ze tridy UI
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace WpfApplicationZyCONTENTGUI
{
    public class CheckboxAndCommand
    {
        public CheckBox CheckBox;
        public int id;
        public int active; //ověření při startu a při odeslání
        public string[] ContentCommands; // pole cisel politk - pro starou verzi usg
        public string[] ApppatrolCommnds; // pole cisel politk - pro starou verzi usg
        public string[] FirewallCommands; // pole cisel politk pro policy control

        /*
        * CheckboxAndCommand 
        * funkce priradi checkBoxu cisla politik na bezpecnostni brane
        */
        public CheckboxAndCommand(CheckBox checkBox, string[] contentCommands, string[] apppatrolCommnds, string[] firewallCommands, int i)
        {
            id = i;
            CheckBox = checkBox;
            ContentCommands = contentCommands;
            ApppatrolCommnds = apppatrolCommnds;
            FirewallCommands = firewallCommands;
        }

    }
}
