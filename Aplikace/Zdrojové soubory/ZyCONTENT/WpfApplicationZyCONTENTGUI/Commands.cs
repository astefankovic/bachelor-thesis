﻿/*

    ZyContent - Spoustec pravidel bezpecnostnich bran ZyXEL

    Class Commands
    Slouzi k definici prikazu zasilanych na bezpecnostni branu
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfApplicationZyCONTENTGUI
{
    class Commands
    {
        public static string desc = "ZyContent_FW_" + WindowContent.room, sourceip = WindowContent.room, serviceName = "ZyContent_wholeWeb";//desc - zacatek nazvu pravidla na FW
        public static string firewall = "firewall";

        /*
         * setFirewall 
         * nastavi dana pravidla na firewallu
         * @param numbers pole cisel, ktera reprezentuji cisla pravidel na bezpecnostni brane
         * @param listQuery seznam prikazu
         */
        public static void setFirewall(string[] numbers, List<string> listQuery, int activate)
        {
            if(activate == 1) //pokud se ma pravidlo zapnout
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    listQuery.Add(firewall + " activate");
                    listQuery.Add(firewall + " " + numbers[i]);
                    listQuery.Add("activate");
                    listQuery.Add("exit");
                }
            }
            else //pokud se ma pravidlo vypnout
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    listQuery.Add(firewall + " activate");
                    listQuery.Add(firewall + " " + numbers[i]);
                    listQuery.Add("no activate");
                    listQuery.Add("exit");
                }
            }
        }

        /*
        * checkFirewall 
        * Zjisti nastaveni bezpecnostni brany
        * @param numbers pole cisel, ktera reprezentuji cisla pravidel na bezpecnostni brane
        */
        public static Boolean checkFirewall(string[] numbers)
        {
            List<string> list = new List<string>();
            Boolean[] set = new Boolean[numbers.Length]; //ulozeni zda jsou pravidla on/off

                for (int i = 0; i < numbers.Length; i++) //pro vsechna pouzivana pravidla
                {
                    if (!numbers[i].Equals(""))
                    {
                        list.Add("show " + firewall + " " + numbers[i]);
                        string[] command = FormControl.listToArray(list);

                        FormControl.justDoIt(command, true);

                        if (Util.commandReturn.Contains(desc) && Util.commandReturn.Contains("status: yes")) //pokud zacina prefixem a je on
                            set[i] = true;
                        else
                            set[i] = false;
                    }
                    else
                        set[i] = false;                    
                }

                if (checkBoolArray(set) == true)//oereni vsech pravidel na jednom checkboxu
                    return true;
                else
                    return false;                      
        }

        /*
        * checkBoolArray 
        * Overi vyplneni pole
        * @param set booleovske pole
        */
        public static Boolean checkBoolArray(Boolean[] set)
        {
            int a = 0;
            for (int i = 0; i < set.Length; i++)
            {
                if(set[i] == true)
                {
                    a++;
                }
            }

            if (a == set.Length)
                return true;
            else
                return false;

        }

    }
}
