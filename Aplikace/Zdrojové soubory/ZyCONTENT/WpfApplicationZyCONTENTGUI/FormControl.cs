﻿/*

    ZyContent - Spoustec pravidel bezpecnostnich bran ZyXEL

    Class FormControl
    Slouzi k ovladani formulare
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Tamir.SharpSsh;
using System.Windows;

namespace WpfApplicationZyCONTENTGUI
{
    class FormControl
    {
        /*
         * setCheckBoxAction 
         * Oznacit nebo odznacit checkBox - osetreni pri vsech oznacenych - musi se odznacit prvni
         */
        public static void setCheckBoxAction(CheckBox actualCheckBox, CheckBox[] checkBoxes)
        {

            if (actualCheckBox.IsChecked == false) //checkBox ktery zavolal funkci je oznaceny
            {
                for (int i = 0; i < checkBoxes.Length; i++)
                {
                    if(checkBoxes[i].Name == "CheckBox_0" && checkBoxes[i].IsChecked == true)
                    {

                        checkBoxes[i].IsChecked = false;
                        break;
                    }
                }
            }
            else //overeni zda je prvni oznaceny - prvni zakazuje ostatni
            {
                int all = 0;
                int sum = 0;

                for (int i = 0; i < checkBoxes.Length; i++)
                {
                    if (checkBoxes[i].Name == "CheckBox_0" && checkBoxes[i].IsChecked == false)
                    {
                        all = 1;
                    }
                    if (checkBoxes[i].Name != "CheckBox_0" && checkBoxes[i].IsChecked == true)
                    {
                        sum += 1;
                    }
                }

                if(all == 1 && sum == (WindowContent.countOfCheckBoxes-1))
                {
                    for (int i = 0; i < checkBoxes.Length; i++)
                    {
                        if (checkBoxes[i].Name == "CheckBox_0" && checkBoxes[i].IsChecked == false)
                        {
                            checkBoxes[i].IsChecked = true;
                        }
                    }

                }
            }
        }

        /*
         * setCheckboxAllAction
         * Oznaceni prvniho checkboxu znamena oznaceni vsech i naopak pri odznaceni prvniho checkboxu
         */
        public static void setCheckboxAllAction(CheckBox actualCheckBox, CheckBox[] checkBoxes)
        {           
            if (actualCheckBox.IsChecked == true) //pokud je oznacen
            {
                for (int i = 0; i < checkBoxes.Length; i++) //smycka pro oznaceni ostatnich
                {
                    checkBoxes[i].IsChecked = true;
                }
            }
            else
            {
                for (int i = 0; i < checkBoxes.Length; i++)
                {
                    checkBoxes[i].IsChecked = false;
                }
            }
        }

        /*
         * countkCheckboxChecked 
         * Vrati pocet oznacenych checkBoxu
         */
        public static int countkCheckboxChecked(CheckBox[] checkBoxes)
        {
            int ret = 0; 
            for (int i = 0; i < checkBoxes.Length; i++)
                {
                    if(checkBoxes[i].IsChecked == true)
                    {
                        ret += 1;
                    }                    
                }
            return ret;            
        }

        /*
         * evaluate - pro tlacitko potvrdit - zapne pravidla
         * Vyhodnoceni zaskrtnutych checkboxu
         */
        public static int evaluate(List<CheckboxAndCommand> checkAndCommand, int a)
        {
            int resultI = 0;//overeni zda je alespon 1 moznost zadana
            listErase(WindowContent.listQuery);
            string result = "";

            if (a == 0)
            {
                MessageBox.Show("Musíte zadat alespoň jednu možnost.", "Neplatná volba", MessageBoxButton.OK, MessageBoxImage.Information);
                return resultI;
            }
            else
            {
                resultI = 1;
                for (int i = 0; i < checkAndCommand.Count; i++)
                {
                    if (checkAndCommand[i].CheckBox.IsChecked == true && checkAndCommand[i].id == 0)
                    {
                        listErase(WindowContent.listQuery);
                        
                            Commands.setFirewall(checkAndCommand[i].FirewallCommands, WindowContent.listQuery, 1);//nastaveni pravidel

                            result = "V této třídě je zakázána veškerá webová komunikace.";
                        break;
                    }           
                    else if (checkAndCommand[i].CheckBox.IsChecked == true)
                    {
                       
                            Commands.setFirewall(checkAndCommand[i].FirewallCommands, WindowContent.listQuery, 1);
                            result = "V této třídě je zakázána vybraná část přístupu k webu.";
                        
                    }
                }

                justDoIt(listToArray(WindowContent.listQuery), false);//provedeni prikazu
                MessageBox.Show(result, "Výsledek", MessageBoxButton.OK, MessageBoxImage.Information);
                return resultI;
            }   
        
        
        }

        /*
         * evaluate - pro tlacitko povolit - pokud jsou politiky zaple
         * Vypne politiky a odznaci checkBoxy
         */
        public static int evaluate(int z, List<CheckboxAndCommand> checkAndCommand, int a)
        {
            int resultI = 0; //overeni zda je alespon 1 moznost zadana
            listErase(WindowContent.listQuery);
            string result = "";

            if (a == 0)
            {
                MessageBox.Show("Musíte zadat alespoň jednu možnost.", "Neplatná volba", MessageBoxButton.OK, MessageBoxImage.Information);
                return resultI;
            }
            else
            {
                resultI = 1;
                for (int i = 0; i < checkAndCommand.Count; i++)
                {
                    if (checkAndCommand[i].CheckBox.IsChecked == true && checkAndCommand[i].id == 0)
                    {
                        listErase(WindowContent.listQuery);

                        Commands.setFirewall(checkAndCommand[i].FirewallCommands, WindowContent.listQuery, 0);//nastaveni pravidel

                        result = "V této třídě je povolena veškerá webová komunikace.";
                        break;
                    }
                    else if (checkAndCommand[i].CheckBox.IsChecked == true)
                    {                        
                            Commands.setFirewall(checkAndCommand[i].FirewallCommands, WindowContent.listQuery, 0);
                            result = "V této třídě je povolena veškerá webová komunikace.";                        
                    }
                }

                justDoIt(listToArray(WindowContent.listQuery), false); //provedeni prikazu
                MessageBox.Show(result, "Výsledek", MessageBoxButton.OK, MessageBoxImage.Information);
                return resultI;
            }


        }

        /*
         * controlsVisibility
         * Znemozni / umozni klikani na checkBoxy
         */
        public static void controlsVisibility(Boolean avaible, Button buttonOK, Button buttonAllow, CheckBox[] checkBoxes)
        {
            if (avaible == true && WindowContent.commandOK == true)
            {
                buttonAllow.Visibility = Visibility.Hidden;
                buttonOK.Visibility = Visibility.Visible;

                for (int i = 0; i < checkBoxes.Length; i++)
                {
                    checkBoxes[i].IsEnabled = true;
                }

            }
            else
            {
                buttonOK.Visibility = Visibility.Hidden;
                buttonAllow.Visibility = Visibility.Visible;

                for (int i = 0; i < checkBoxes.Length; i++)
                {
                    checkBoxes[i].IsEnabled = false;
                }
            }
        }

        /*
         * listToArray
         * Prevede List na pole
         */
        public static string[] listToArray(List<string> list)
        {
            string[] array = list.ToArray();
            return array;
        }

        /*
         * listToArray
         * Prevede List na pole
         */
        public static CheckBox[] listToArray(List<CheckBox> list)
        {
            CheckBox[] array = list.ToArray();
            return array;
        }


        /*
         * listErase
         * Vymaze list
         */
        public static void listErase(List<string> listQuery)
        {
            listQuery.Clear();
        }

        /*
         * justDoIt
         * Posle prikaz pomoci tridy Util
         */
        public static void justDoIt(String[] commands, Boolean gettext)
        {
            if (Util.IsAvaible(WindowContent.host))
            {
                String status = "Zařízení je dosažitelné.";
                Util.commandReturn = "";

                WindowContent.ssh = Util.SshConnect(WindowContent.host, WindowContent.user, WindowContent.pswd);//pripojeni
                status += "\n" + Util.commandReturn;

                //ověření jestli se podařilo připojit - určení kde přesně je chyba   
                if (Util.commandReturn == "SSH spojení bylo navázáno.")
                {
                    Util.doCommand(WindowContent.ssh, gettext, true, commands, false); //provedeni prikazu
                    Util.SshDisconnect(WindowContent.ssh); //odpojeni

                }
                else 
                {
                    MessageBox.Show("Litujeme, ale k zařízení se nelze připojit. \nPro nápravu kontaktujte administrátora!\n\nAplikace bude ukončena.\nERROR: " + WindowContent.host + " CANNOT MAKE SSH CONNECTION", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Error);

                    StatusCheck.saveToLog("Neplatný pokus o připojení k bezpečnostní bráně", "K IP adrese se nepdařilo připojit.");

                    Environment.Exit(0);
                }
            }
            else
            {
                MessageBox.Show("Litujeme, ale zařízení právě není dosažitelné. \nPro nápravu kontaktujte administrátora!\n\nAplikace bude ukončena.\nERROR: " + WindowContent.host + " NOT PINGING", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Error);

                StatusCheck.saveToLog("Neplatný pokus o připojení k bezpečnostní bráně", "IP adresa není dosažitelná.");
                
                Environment.Exit(0);
            }
        }
    }
}
