﻿/*

    ZyContent - Spoustec pravidel bezpecnostnich bran ZyXEL

    Class GetText
    Slouzi k nacitani radku ze souboru a k jeho parsovani
        
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationZyCONTENTGUI
{
    class GetText
    {
        public static string[] linesFromFile;//pole ve kterem jsou ulozeny radky ze souboru


        /*
        * readFile 
        * načte text do pole - co řádka v .txt to položka v poli
        */
        public static void readFile(String fileName)
        {
            linesFromFile = System.IO.File.ReadAllLines(WindowContent.path + fileName, Encoding.Default);
        }

        /*
         * between 
         * parsovani textu - obsah mezi dvema retezci
         */
        public static string between(string value, string a, string b)
        {
            int posA = value.IndexOf(a);
            int posB = value.LastIndexOf(b);
            if (posA == -1)
            {
                return "";
            }
            if (posB == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= posB)
            {
                return "";
            }
            return value.Substring(adjustedPosA, posB - adjustedPosA);
        }

        /*
          * after 
          * parsovani textu - obsah za retezcem
          */
        public static string after(string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }

        /*
         * Before 
         * parsovani textu - obsah pred retezcem
         */
        public static string Before(string value, string a)
        {
            int posA = value.IndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            return value.Substring(0, posA);
        }

        /*
         * firstChar 
         * Ziska prvni znak z retezce
         */
        public static char firstChar(string a)
        {
            return a.ToCharArray()[0];
        }

        /*
         * secondChar 
         * Ziska druhy znak z retezce
         */
        public static char secondChar(string a)
        {
            return a.ToCharArray()[1];
        }

        /*
         * getChar 
         * Ziska znak pro ucely parsovani
         */
        public static char getChar(string a, int b)
        {
            return a.ToCharArray()[b];
        }

        /*
         * stringLength 
         * Ziska delku retezce
         */
        public static int stringLength(string a)
        {
            return a.ToCharArray().Length;
        }

        /*
         * DecodeFromUtf8 
         * Dekoduje z UTF8
         */
        public static string DecodeFromUtf8(string utf8_String)
        {
            //string utf8_String = "dayâ€™s";
            byte[] bytes = Encoding.Default.GetBytes(utf8_String);
            return utf8_String = Encoding.UTF8.GetString(bytes);
        }
    }
}
