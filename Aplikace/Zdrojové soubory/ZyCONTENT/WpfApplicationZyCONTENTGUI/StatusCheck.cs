﻿/*

    ZyContent - Spoustec pravidel bezpecnostnich bran ZyXEL

    Class StatusCheck
    Slouzi k ziskani informaci o stavu bezpecnostni brany
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace WpfApplicationZyCONTENTGUI
{
    class StatusCheck
    {
        /*
       * checkItAll 
       * Zkontroluje vsechna nastaveni bezpecnostni brany
       */
        public static void checkItAll(List<CheckboxAndCommand> checkAndCommand, Button buttonOK, Button buttonAllow)
        {
            Boolean[] firewall = checkFirewallSettings(checkAndCommand);//ulozena informace zda je pouzivane pravidlo on/off

            for(int i = 0; i < firewall.Length; i++)
            {
                if (firewall[i] == true)
                {
                    if(i==0)//pokud je zapnute pravidlo na prvnim checkboxu
                    {
                        for (int j = 0; j < WindowContent.checkBoxes.Count; j++)//vsechny checkboxy se zaskrtnou
                        {
                            WindowContent.checkBoxes[j].IsChecked = true; 
                        }
                        FormControl.controlsVisibility(false, buttonOK, buttonAllow, FormControl.listToArray(WindowContent.checkBoxes));
                        break;

                    }
                    
                    //zaskrtnou se specificke checkboxy
                    checkAndCommand[i].CheckBox.IsChecked = true;
                    FormControl.controlsVisibility(false, buttonOK, buttonAllow, FormControl.listToArray(WindowContent.checkBoxes));
                }      
            }
        }

        /*
       * checkFirewallSettings 
       * Overi nastaveni bezpescnostni brany
       */
        public static Boolean[] checkFirewallSettings(List<CheckboxAndCommand> checkAndCommand)
        {
            Boolean[] numbersArray = new Boolean[WindowContent.countOfCheckBoxes];//pro ulozeni informace zda je pouzivane pravidlo on/off

            for (int i = 0; i < checkAndCommand.Count; i++)
            {
                    if (Commands.checkFirewall(checkAndCommand[i].FirewallCommands) == true)
                        numbersArray[i] = true;
                    else
                        numbersArray[i] = false;
                
            }
            
            return numbersArray;
        }


       /*
       * saveToLog
       * Ulozi do log.txt
       */
        public static void saveToLog(String description, String detail)
        {
            //zapis do logu

            try
            {
                DateTime datumCas = DateTime.Now;
                System.IO.StreamWriter file = new System.IO.StreamWriter(WindowContent.path + WindowContent.logTXT, true);
                file.WriteLine(System.Environment.NewLine + "{0}", datumCas);
                file.WriteLine("\n\t-----> " + description);
                file.Write("\n\t\t " + detail);

                file.Close();
            }
            catch
            {
                MessageBox.Show("Nepodařilo se zapsat do logu, kontaktuje prosím správce sítě. Můžete pokračovat ve své činnosti.", "Přístup k logu byl odepřen", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
           
        }



    }
}
