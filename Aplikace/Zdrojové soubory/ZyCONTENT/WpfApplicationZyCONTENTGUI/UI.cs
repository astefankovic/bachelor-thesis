﻿/*

    ZyContent - Spoustec pravidel bezpecnostnich bran ZyXEL

    Class UI
    Slouzi k obsluze uzivatelskeho prostedi
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tamir.SharpSsh;

namespace WpfApplicationZyCONTENTGUI
{
    class UI
    {

        /*
         * createCheckBox 
         * Vytvori checkBox s danymi parametry
         */
        public static CheckBox createCheckBox(StackPanel Layout, Style style, string name, string text, string toolTip, RoutedEventHandler handlerClick)
        {
            CheckBox box = null;

            box = new CheckBox();
            box.Name = name;
            box.Style = style;

            box.Click += new RoutedEventHandler(handlerClick);
            box.LayoutTransform = new ScaleTransform(1.5, 1.5);
            TextBlock textBlck = new TextBlock();

            Run run1 = new Run(text); //nazev checkboxu
            Run run1_ = new Run(" ");
            Run run2 = new Run("?"); //otaznik za textem = co vse politika zakaze

            Hyperlink hyperlink = new Hyperlink(run2) //tooltip - najeti na otaznik
            {                
                ToolTip = toolTip
            };
            
            var converter = new System.Windows.Media.BrushConverter();
            var brush = (Brush)converter.ConvertFromString("#3babe3"); //modra barva otazniku
            hyperlink.Foreground = brush;
            hyperlink.FontWeight = FontWeights.Bold;
            hyperlink.TextDecorations = null;

            //pridani vsech textovych casti v popisu zaskrtavaciho tlacitka
            textBlck.Inlines.Clear();
            textBlck.Inlines.Add(run1);
            textBlck.Inlines.Add(run1_);
            textBlck.Inlines.Add(hyperlink);

            box.Content = textBlck;

            Layout.Children.Add(box);

            return box;
        }

        /*
         * prepareCheckBoxes 
         * Pripravi checkBoxy - prida do nich informace z konfiguracnich souboru
         */
        public static void prepareCheckBoxes(StackPanel firstLayout, StackPanel secondLayout, Style style)
        {
            int count = 0;
            int countOfHashtags = 0;
            List<string> ContentCommands = new List<string>(), AppatrolCommands = new List<string>(), FirewallCommands = new List<string>();

                for(int i = 0; i < GetText.linesFromFile.Length -1; i++)
                {
                        string line = cipher.DecryptStr(GetText.linesFromFile[i], cipher.myRijndaelKey, cipher.myRijndaelIV);
                        string text = "", tooltip = "";
                        int ok = 0;
                        FormControl.listErase(ContentCommands);
                        FormControl.listErase(AppatrolCommands);
                        FormControl.listErase(FirewallCommands);

                        if (GetText.firstChar(line) == '#' && countOfHashtags < WindowContent.maximumOfCheckBoxes) //# slouzi jako identifikator noveho
                        {
                            countOfHashtags += 1; //overeni maximalniho poctu zaskrtavacich policek
                            text = line.Substring(1);
                            ok = 1;
                            //parsovani konfiguracniho souboru
                            if (i != (GetText.linesFromFile.Length - 4))
                            {

                                //text je potreba desiforvat
                                string nextTool = cipher.DecryptStr(GetText.linesFromFile[i + 1], cipher.myRijndaelKey, cipher.myRijndaelIV);
                                string nextF = cipher.DecryptStr(GetText.linesFromFile[i + 2], cipher.myRijndaelKey, cipher.myRijndaelIV); //vice pravidel pro jedno tlacitko

                                if (GetText.firstChar(nextTool) == '@')//Tooltip
                                {
                                    tooltip = nextTool.Substring(1);
                                }

                                if (GetText.firstChar(nextF) == '&' && GetText.secondChar(nextF) == 'F')//FW pravidla na bezpecnostni brane
                                {
                                    int lineLength = GetText.stringLength(nextF);
                                    string number = "";
                                    for (int z = 2; z < lineLength; z++)
                                    {
                                        char a = GetText.getChar(nextF, z);//jednomistne cislo
                                        char b = ' ';//dvoumistne cislo
                                        char c = ' ';//trojmistne cislo
                                        int places = 0;
                                        int places3 = 0;
                                        if(z < (lineLength - 1))
                                        {
                                            b = GetText.getChar(nextF, z + 1);
                                            if (Char.IsDigit(b))
                                                {
                                                    places = 1;

                                                }                                      
                                        }

                                        if (z < (lineLength - 2))
                                        {
                                            c = GetText.getChar(nextF, z + 2);
                                            if (Char.IsDigit(c) && b != '-')
                                            {
                                                places3 = 1;

                                            }
                                        }  

                                        if (a != '-' && a != ' ' && a != '\0' && places == 0 && places3 == 0)
                                        {
                                            FirewallCommands.Add(a.ToString());//prirazeni pravidel k tlacitku
                                        }
                                        else if (a != '-' && a != ' ' && a != '\0' && places == 1 && places3 == 1)
                                        {
                                            number = a.ToString() + b.ToString() + c.ToString() ;
                                            FirewallCommands.Add(number);//prirazeni pravidel k tlacitku
                                            z += 2;
                                        }
                                        else if (a != '-' && a != ' ' && a != '\0' && places == 1)
                                        {
                                            number = a.ToString() + b.ToString();
                                            FirewallCommands.Add(number);//prirazeni pravidel k tlacitku
                                            z++;

                                        }


                                    }
                                }
                            }
                        }
                        //vlastni pridani checkBoxu
                        if (ok == 1 && count == 0)//prvni checkbox
                        {
                            WindowContent.checkBoxes.Add(createCheckBox(firstLayout, style, "CheckBox_" + i, text, tooltip, WindowContent.CheckBoxMain_click));//nazvy tlacitek se inkrementuji
                            CheckBox a = new CheckBox();
                            CheckboxAndCommand c = new CheckboxAndCommand(WindowContent.checkBoxes.Last(), FormControl.listToArray(ContentCommands), FormControl.listToArray(AppatrolCommands), FormControl.listToArray(FirewallCommands), i);

                            WindowContent.checkAndComm.Add(c);//pridani na formulare
                            WindowContent.countOfCheckBoxes++;
                        }
                        else if (ok == 1)
                        {
                            WindowContent.checkBoxes.Add(createCheckBox(secondLayout, style, "CheckBox_" + i, text, tooltip, WindowContent.CheckBox_click));//nazvy tlacitek se inkrementuji
                            CheckBox a = new CheckBox();
                            CheckboxAndCommand c = new CheckboxAndCommand(WindowContent.checkBoxes.Last(), FormControl.listToArray(ContentCommands), FormControl.listToArray(AppatrolCommands), FormControl.listToArray(FirewallCommands), i);

                            WindowContent.checkAndComm.Add(c);//pridani na formulare
                            WindowContent.countOfCheckBoxes++;
                        }

                        count += 1;         

                                                
                }                
        }      

    }    
}
