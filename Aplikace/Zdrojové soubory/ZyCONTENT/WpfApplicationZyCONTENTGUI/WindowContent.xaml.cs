﻿/*

    ZyContent - Spoustec pravidel bezpecnostnich bran ZyXEL

    Class WindowContent
    Slouzi k definici uzivatelskeho prostredi
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tamir.SharpSsh;
using System.Threading;
using System.IO;

namespace WpfApplicationZyCONTENTGUI
{
    public partial class WindowContent : Window
    {

        //inicializace promennych
        public static string host = ""; // IP adresa fw
        public static string user = ""; // ssh prihlasovani
        public static string pswd = ""; // ssh prihlasovani
        public static string room = ""; // nazev pc ucebny - rozsah ip adres
        public static string machine = ""; // vlastní unikatni nazev fw
        public static string versionUSG = "new"; //old x new
        public static int countOfCheckBoxes = 0; // pocet checkBoxu
        public static int authorized = 0; // prihlaseni
        public static SshShell ssh;
        public static List<string> listQuery = new List<string>();
        public static string path = System.IO.Directory.GetCurrentDirectory(); //cesta spusteni.exe
        public static string configTXT = @"\config\connection.txt";
        public static string configUI = @"\config\ui.txt";
        public static string logTXT = @"\config\log.txt";
        public static Boolean all = false, social = false, games = false, email = false, sms = false;
        public static Boolean commandOK = false;
        public static List<CheckBox> checkBoxes = new List<CheckBox>();//list checkboxu
        public static List<CheckboxAndCommand> checkAndComm = new List<CheckboxAndCommand>(); //pravidla prirazena k checkboxu
        public static string diffieDll = @"\DiffieHellman.dll";
        public static string schedDll = @"\Microsoft.Win32.TaskScheduler.dll";
        public static string secDll = @"\Org.Mentalis.Security.dll";
        public static string sharpSSHDll = @"\Tamir.SharpSSH.dll";
        public static int maximumOfCheckBoxes = 6;

        public WindowContent()
        {
            InitializeComponent();
        }

        /*
        * Window_Loaded 
        * funkce ktera udela vsechny ukoly po nacteni okna
        */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //načíst připojovací údaje
           try
           {
               this.Title = "Komunikuji s bezpečnostní branou ... (přibližně 30s)";//info pro uzivatele

               Mouse.OverrideCursor = Cursors.Wait;

               //Overeni existence knihoven
               if (!File.Exists(path + diffieDll) || !File.Exists(path + schedDll) || !File.Exists(path + secDll) || !File.Exists(path + sharpSSHDll))
               {
                   MessageBox.Show("Knihovny byly smazány, program nemůže být načten! \nERROR: .DLL IS MISSING", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                   StatusCheck.saveToLog("Neplatný pokus o spuštění programu", "Neexistence knihoven.");

                   Environment.Exit(0);
               }

                string configFileHash = cipher.HashSHA1(path + configTXT);
                string uiFileHash = cipher.HashSHA1(path + configUI);

               //overeni zda nebylo zasazeno do konfiguracnich souboru
                GetText.readFile("\\hcheck.txt");
                

                if (configFileHash != GetText.linesFromFile[0])
                {
                    MessageBox.Show("Poškozena integrita dat! Konfigurační soubory byly změněny, program nemůže být načten!\nERROR: HCHECK IS BAD", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                    StatusCheck.saveToLog("Neplatný pokus o spuštění programu", "Poškozena integrita dat - hcheck.");
              
                    Environment.Exit(0);
                }
                

                GetText.readFile("\\uicheck.txt");
                if (uiFileHash != GetText.linesFromFile[0])
                {
                    MessageBox.Show("Poškozena integrita dat! Konfigurační soubory byly změněny, program nemůže být načten! \nERROR: UICHECK IS BAD", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                    StatusCheck.saveToLog("Neplatný pokus o spuštění programu", "Poškozena integrita dat - uicheck.");

                    Environment.Exit(0);
                }
                

                GetText.readFile(configTXT);

               //ulozeni pripojovacich udaju
                host = cipher.DecryptStr(GetText.linesFromFile[0], cipher.myRijndaelKey, cipher.myRijndaelIV);
                user = cipher.DecryptStr(GetText.linesFromFile[1], cipher.myRijndaelKey, cipher.myRijndaelIV);
                pswd = cipher.DecryptStr(GetText.linesFromFile[2], cipher.myRijndaelKey, cipher.myRijndaelIV);
                machine = cipher.DecryptStr(GetText.linesFromFile[3], cipher.myRijndaelKey, cipher.myRijndaelIV);
                room = cipher.DecryptStr(GetText.linesFromFile[5], cipher.myRijndaelKey, cipher.myRijndaelIV);
                //versionUSG = cipher.DecryptStr(GetText.linesFromFile[6], cipher.myRijndaelKey, cipher.myRijndaelIV);

                if(versionUSG == "old")
                {
                    Commands.firewall = "firewall";
                }
                else if(versionUSG == "new")
                {
                    Commands.firewall = "secure-policy";
                }
                labelHeader.Content = "Filtrování webového obsahu pro učebnu " + room;               

                GetText.readFile(configUI);
                UI.prepareCheckBoxes(firstLayout, secondLayout, (Style)FindResource("CheckBoxStyle"));

                //MessageBox.Show(host + " . " + user + " . " + pswd + " . " + room + " . " + versionUSG);
                StatusCheck.checkItAll(checkAndComm, buttonOK, buttonAllow);
               
                //buttonOK.Visibility = Visibility.Visible;
                this.Title = "ZyXEL CONTENT FILTR [" + machine + "]";
                Mouse.OverrideCursor = null;

            }
             catch
             {
                 MessageBox.Show("Důležité soubory s nastavením byly smazány.\nObnovte je prosím. \n\nPro opravu kontaktuje správce sítě. Aplikace bude vypnuta.\nERROR: .TXT IS MISSING", "Poškození souborů", MessageBoxButton.OK, MessageBoxImage.Hand);

                 StatusCheck.saveToLog("Neplatný pokus o spuštění programu", "Poškozena integrita dat, vadné konfigurační soubory.");
                
                 Environment.Exit(0);
             }
        }


        /*
        * buttonAllow_Click 
        * funkce ktera zobrazi tlacitko pro povoleni politik po jejich zakazani 
        */
        private void buttonAllow_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            FormControl.evaluate(1, checkAndComm, FormControl.countkCheckboxChecked(FormControl.listToArray(checkBoxes)));
            FormControl.controlsVisibility(true, buttonOK, buttonAllow, FormControl.listToArray(checkBoxes));
            Mouse.OverrideCursor = null;
        }

        /*
        * buttonOK_Click 
        * funkce pro obsluhu tlacitka pro vykonani vsech pozadavku
        */
        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            if(FormControl.evaluate(checkAndComm, FormControl.countkCheckboxChecked(FormControl.listToArray(checkBoxes))) == 1)
                FormControl.controlsVisibility(false, buttonOK, buttonAllow, FormControl.listToArray(checkBoxes));
            
            Mouse.OverrideCursor = null;
        }

        /*
        * CheckBox_click 
        * funkce ktera kotroluje ztisknuti jakehokoliv checkboxu
        */
        public static void CheckBox_click(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            FormControl.setCheckBoxAction(checkBox, FormControl.listToArray(checkBoxes));
        }

        /*
        * CheckBoxMain_click 
        * funkce pro prvni checkBox
        */
        public static void CheckBoxMain_click(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            FormControl.setCheckboxAllAction(checkBox, FormControl.listToArray(checkBoxes));
        }

    }   
}
