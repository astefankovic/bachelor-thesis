﻿/*

    ZyREBOOT - Aplikace restartujici bezpecnostni bran ZyXEL

    Class Program
    Hlavni trida apliakce obsluha chodu programu
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamir.SharpSsh;
using System.Threading;
using System.IO;



namespace ConsoleApplication1
{
    class Program
    {
        //incializace promennych
        public static string host = ""; //IP adresa FW
        public static string user = ""; //pro SSH
        public static string pswd = ""; //pro SSH
        public static string smtpServer = "smtp.gmail.com"; //SMTP SERVER
        public static string FromEmail = "zyreboot@gmail.com"; //EMAILOVY UCET
        public static string FromEmailPSWD = "ZyxelReb00t"; //HESLO K EMAILU
        public static string ToEmail = ""; //KOMU ODESLAT
        public static string identifikator = ""; //unikatni nazev FW
        public static SshShell ssh;
        public static string returnText;
        public static string allErrors = "";//string plny error logu
        public static string allOK = "";//string plny ok stavu
        public static string path = System.IO.Directory.GetCurrentDirectory(); //AKTUALNI ADRESAR
        public static string configTXT = @"\config\connection.txt";
        public static string ipaddressesTXT = @"\config\ipaddress.txt";
        public static string logTXT = @"\config\log.txt";
        public static string srvEXE = @"\ZyREBOOTnow.exe";
        public static string diffieDll = @"\DiffieHellman.dll";
        public static string schedDll = @"\Microsoft.Win32.TaskScheduler.dll";
        public static string secDll = @"\Org.Mentalis.Security.dll";
        public static string sharpSSHDll = @"\Tamir.SharpSSH.dll";

        /*
        * Main 
        * funkce main - kompletni logika programu
        */
        static void Main(string[] args)
        {
            //nacteni pripojovacich udaju
            try
            {
                //overeni existence knihoven
                if (!File.Exists(path + diffieDll) || !File.Exists(path + schedDll) || !File.Exists(path + secDll) || !File.Exists(path + sharpSSHDll))
                {
                    Console.WriteLine("Knihovny nemohly být načteny, ověřte prosím existenci souborů " + diffieDll + ", " + schedDll + ", " + secDll + ", " + sharpSSHDll);
                    
                    Util.saveToLog("Neplatný pokus o spuštění reboot programu", "Knihovny nemohly být načteny, ověřte prosím existenci souborů");
                    
                    Thread.Sleep(1 * 60 * 1000);//pozastaveni aplikace
                    Environment.Exit(0);
                }
                
                string configFileHash = cipher.HashSHA1(path + configTXT);
                getText.readFile("\\hcheck.txt");

                

                if (configFileHash != getText.linesFromFile[0])
                {
                    Console.WriteLine("Poškozena integrita dat! Konfigurační soubory byly změněny, program nemůže být načten!");

                    Util.saveToLog("Neplatný pokus o spuštění reboot programu", "Poškozena integrita dat, reboot nemohl proběhnout.");

                    Thread.Sleep(1 * 60 * 1000); //pozastevni aplikace
                    Environment.Exit(0);
                }

                getText.readFile(configTXT);
                
                host = cipher.DecryptStr(getText.linesFromFile[0], cipher.myRijndaelKey, cipher.myRijndaelIV);
                user = cipher.DecryptStr(getText.linesFromFile[1], cipher.myRijndaelKey, cipher.myRijndaelIV);
                pswd = cipher.DecryptStr(getText.linesFromFile[2], cipher.myRijndaelKey, cipher.myRijndaelIV);
                identifikator = cipher.DecryptStr(getText.linesFromFile[3], cipher.myRijndaelKey, cipher.myRijndaelIV);
                ToEmail = cipher.DecryptStr(getText.linesFromFile[4], cipher.myRijndaelKey, cipher.myRijndaelIV);
            }
            catch
            {
                Console.WriteLine("Důležité soubory s nastavením byly smazány.\nObnovte prosím \n\t\\config\\ipaddress.txt a \\config\\connection_and_time.txt hchceck.txt. \nAplikace bude vypnuta, záznam se uloží do logu.");

                Util.saveToLog("Neplatný pokus o spuštění reboot programu", "Byly poškozeny důležité soubory, reboot nemohl proběhnout.");

                Thread.Sleep(1 * 60 * 1000);//pozastaveni aplikace
                Environment.Exit(0);
            }
            

            Console.WriteLine("---------ZPRÁVA O REBOOTU NA ADRESE " + host + "---------");

            
            //pokus o reboot
            try 
            
            {
                if (Util.IsAvaible(host)) //ping
                {
                    ssh = Util.SshConnect(host, user, pswd);
                    Util.doCommand(ssh, false, false, "reboot", false);
                    Util.SshDisconnect(ssh);
                }
                else
                {
                    returnText += "\n\nZařízení je NEDOSTUPNÉ, reboot nemohl být proveden.";
                    allErrors = "\t\t-----> Reboot nemohl být proveden, zařízení bylo v danou chvíli nedostupné.";
                } 
            }
            catch 
            {
                returnText += "\n\nZařízení je NEDOSTUPNÉ,  IP adresa není v platném tvaru.";
                allErrors = "\t\t-----> Reboot nemohl být proveden, IP adresa není v platném tvaru.";

                ////ČASOVAČ
                Thread.Sleep(1 * 60 * 1000);
                Environment.Exit(0);
            }
                   

             //uspání
             Console.Write(returnText);
             Console.WriteLine("\n[" + DateTime.Now.AddMinutes(10) + "] proběhne ping nastavených IP adres.");
             returnText = "";
            
            //ČASOVAČ
            Thread.Sleep(10 * 60 * 1000);

            try
            {
                //načtení IP adres na propingování
                getText.readFile(ipaddressesTXT);
            }
            catch
            {
                Console.WriteLine("Důležité soubory s nastavením byly smazány.\nObnovte prosím \n\t\\config\\ipaddress.txt a \\config\\connection_and_time.txt. \nAplikace bude vypnuta, záznam se uloží do logu.");

                Util.saveToLog("Během rebootu se vyskytly problémy", "Není možné otestovat IP adresy.");

                ////ČASOVAČ
                Thread.Sleep(1 * 60 * 1000);
                Environment.Exit(0);
            }           
          
            returnText += "\n\n\n---------Test dostupnosti nastavených IP adres---------";
            Console.WriteLine("\n\n---------Test dostupnosti nastavených IP adres---------");

            foreach (string line in getText.linesFromFile) //propingovani adres ze souboru ipaddress.txt
            {              
                returnText += "\n\t" + line;
                Console.WriteLine("\n\t" + line);


                if (line != "")
                {
                    if (Util.IsAvaible(line) == true)//vyhodnoceni pingu
                    {
                        returnText += "\n\t\t adresa je dostupná";
                        Console.WriteLine("\n\t\t adresa je dostupná");
                        allOK += System.Environment.NewLine + "\t\t-----> Adresa " + line + " je dostupná.";
                    }
                    else
                    {
                        returnText += "\n\t\t ADRESA NENÍ DOSTUPNÁ";
                        Console.WriteLine("\n\t\t ADRESA NENÍ DOSTUPNÁ");
                        allErrors += System.Environment.NewLine + "\t\t-----> Adresa " + line + " je NEDOSTUPNÁ.";
                    }
                }
            }

            //vyhodnocení    
            if(allErrors == "")//pokud nenastal problem
            {
                DateTime datumCas = DateTime.Now;

                System.IO.StreamWriter file = new System.IO.StreamWriter(path + logTXT, true);
                file.WriteLine("\n{0}", datumCas);
                file.WriteLine("\n\t-----> Reboot proběhl v pořádku.");

                file.Close();

                DateTime datumCasEmail = DateTime.Now;
                email.sendEmail("Zpráva o rebootu " + identifikator + " ze dne " + datumCasEmail, "Reboot proběhl v pořádku."); //odeslani e-mailu
            }
            else 
            {
                DateTime datumCas = DateTime.Now;

                System.IO.StreamWriter file = new System.IO.StreamWriter(path + logTXT, true);
                file.WriteLine(System.Environment.NewLine + "{0}", datumCas);
                file.WriteLine("\n\t-----> Během rebootu se vyskytly problémy");
                file.Write(allErrors);
                file.Write(allOK);

                file.Close();


                DateTime datumCasEmail = DateTime.Now;
                email.sendEmail("ALERT zpráva o rebootu " + identifikator + " ze dne " + datumCasEmail, "Během rebootu se vyskytly problémy\n" + allErrors + allOK); //odeslani e-mailu
            }

            //výpis na konzoli
           // Console.Write(returnText);
            Console.Write("\nUdálost byla zapsána do /config/log.txt.\n " + DateTime.Now.AddMinutes(10) + "] bude program vypnut, opětovně se zapne\npokud máte nastavené opakované spouštění v Plánovači úloh");

            //System.Console.ReadKey();

            ////ČASOVAČ
            Thread.Sleep(1 * 60 * 1000); 
            
            System.Environment.Exit(0);

        }

    }
    
}



