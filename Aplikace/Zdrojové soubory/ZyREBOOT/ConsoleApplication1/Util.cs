﻿/*

    ZyREBOOT - Aplikace restartujici bezpecnostni bran ZyXEL

    Class cipher
    Slouzi k sifrovani a desifrovani dat
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections;
using Tamir.SharpSsh;
using System.Windows;
using System.Threading;
using System.Net.NetworkInformation;

namespace ConsoleApplication1
{
    // Util slouží pro práci s ssh
    public class Util
    {
        public static string commandReturn = ""; //globalni promenna pro ulozeni vystupu
        public static int countOfstandard = 1; //slouzi pro cteni z konzole
        public static Statement connectStatement = new Statement(); //info o pripojeni

        /*
         * IsAvaible 
         * funkceoveri dostupnost dane ip adresy
         * @param host ip adresa bezpecnostni brany
         */
        public static bool IsAvaible(string host)
        {
            try
            {
                Ping p = new Ping();
                PingReply r;
                string ip = host;
                r = p.Send(ip);

                if (r.Status == IPStatus.Success)
                    return true;
                else
                    return false;
            }

            catch
            {
                Console.WriteLine("\nNení možné pingovat IP adresu " + host);
                return false;
            }
          
        }

        /*
         * SshConnect 
         * Pripojeni k serveru
         */
        public static SshShell SshConnect(string host, string user, string pswd)
        {
            commandReturn = "";
            SshConnectionInfo input = Util.GetInput(host, user, pswd);
            SshShell ssh = new SshShell(input.Host, input.User);
            if (input.Pass != null)
                ssh.Password = input.Pass;

            try
            {
                ssh.Connect();

                connectStatement.statementResult = "Připojeno";                
                commandReturn = ("SSH spojení bylo navázáno.");
                Program.returnText += ("\nSSH spojení bylo navázáno.");
            }
            catch
            {
                connectStatement.statementResult = "SSH spojení se nepodařilo. ERROR(SshConnect catch)";
                commandReturn = ("SSH připojení nemohlo být navázáno. Zkuste jiné přihlašovací údaje a ověřte, že se skutečně jedná o kompatibilní zařízení ZyXEL.");
                Program.returnText += ("\nSSH připojení nemohlo být navázáno. Zkuste jiné přihlašovací údaje a ověřte, že se skutečně jedná o kompatibilní zařízení ZyXEL.");
                Program.allErrors += System.Environment.NewLine + "\t\t-----> Chyba se vyskytla během vytváření SSH spojení.";
            }

            return ssh;
        }

        /*
       * SshDisconnect 
       * Odpojeni od serveru
       */
        public static void SshDisconnect(SshShell ssh)
        {
            try
            {
                ssh.Close();
                connectStatement.statementResult = "Spojení bylo ukončeno";
                Program.returnText += "\nSpojení bylo ukončeno.";
            }
            catch
            {
                connectStatement.statementResult = "Spojení nemohlo být řádně ukončeno, zařízení je již pravděpodobně nedostupné. ERROR(SshDisconnect catch)";
                Program.returnText += "\nSpojení nemohlo být řádně ukončeno, zařízení je již pravděpodobně nedostupné. ERROR(SshDisconnect catch)";
                Program.allErrors += System.Environment.NewLine + "\t\t-----> Chyba se vyskytla během ukončení SSH spojení.";
            }
        }

        /*
          * doCommand 
          * Provedeni prikazu
          */
        public static void doCommand(SshShell ssh, Boolean getMessage, Boolean configMode, String command, Boolean needWrite)
        {
            commandReturn = "";

            if (ssh.ShellOpened)
            {
                string pattern;    //očekávaný znak příkazové řádky (standartně #, >)            

                try
                {
                    //zajištění přechodu do konfiguračního módu
                    if (configMode == true)
                    {
                        pattern = "#";
                        ssh.WriteLine("configure terminal");
                    }
                    else
                        pattern = ">";

                    
                        ssh.Expect(pattern);

                    ssh.ExpectPattern = pattern;
                    ssh.RemoveTerminalEmulationCharacters = true;

                    //provedení příkazu
                    ssh.WriteLine(command);

                    //pokud je potřeba zajištění uložení zprávy od serveru
                    if (getMessage == true && configMode == true)
                    {
                        Thread.Sleep(500); //kvůli komunikaci se musí chvíli počkat
                        commandReturn = ssh.Expect(pattern);
                    }
                    else if (getMessage == true && configMode != true)
                    {
                        Thread.Sleep(500);
                        commandReturn = ssh.Expect(pattern);
                    }
                    else if (getMessage == false && configMode == true)
                    {
                        Thread.Sleep(500);
                        commandReturn = "Příkaz byl úspěšně proveden.";
                        Program.returnText += "\nPříkaz byl úspěšně proveden.";
                    }
                    else if (getMessage == false && configMode == false)
                    {
                        Thread.Sleep(500);
                        commandReturn = "Příkaz byl úspěšně proveden.";
                        Program.returnText += "\nPříkaz byl úspěšně proveden.";
                    }

                    if (needWrite == true)
                        ssh.WriteLine("write");


                }
                catch
                {
                    commandReturn = "Nastala chyba v příkazu, je možné že zařízení nepodporuje daný příkaz. ERROR(doCommand catch)";
                    Program.returnText += "\nNastala chyba v příkazu, je možné že zařízení nepodporuje daný příkaz. ERROR(doCommand catch)";
                    Program.allErrors += System.Environment.NewLine + "\t\t-----> Chyba se vyskytla během příkazu REBOOT.";
                }
            }
            else
            {
                commandReturn = ("SSH příkaz se nepodařilo provést. Zkuste jiné přihlašovací údaje a ověřte, že se skutečně jedná o kompatibilní zařízení ZyXEL.");
                Program.returnText += ("\nSSH příkaz se nepodařilo provést.");
                Program.allErrors += System.Environment.NewLine + "\t\t-----> REBOOT se nepodařilo provést.";
            }

        }

        /*
         * GetInput 
         * Slouzi pro komunikaci se serverem - init data
         */
        public static SshConnectionInfo GetInput(string host, string user, string pswd)
        {
            SshConnectionInfo info = new SshConnectionInfo();

            info.Host = host;
            info.User = user;
            info.Pass = pswd;

            return info;
        }

        /*
     * saveToLog
     * Ulozi do log.txt
     */
        public static void saveToLog(String description, String detail)
        {
            //zapis do logu

            try
            {
                DateTime datumCas = DateTime.Now;
                System.IO.StreamWriter file = new System.IO.StreamWriter(Program.path + Program.logTXT, true);
                file.WriteLine(System.Environment.NewLine + "{0}", datumCas);
                file.WriteLine("\n\t-----> " + description);
                file.Write("\n\t\t " + detail);

                file.Close();
            }
            catch
            {
                Console.WriteLine("Nepodařilo se zapsat do logu, kontaktuje prosím správce sítě. Můžete pokračovat ve své činnosti.");
            }

        }
    }

    /*
    * SshConnectionInfo 
    * Struktura uklada init data pro pripojeni
    */
    public struct SshConnectionInfo
    {
        public string Host;
        public string User;
        public string Pass;
        public string IdentityFile;
    }

    /*
    * SshConnectionInfo 
    * Infomace o stavovem radku
    */
    public struct Statement
    {
        public string statementResult;
        
    }
}
