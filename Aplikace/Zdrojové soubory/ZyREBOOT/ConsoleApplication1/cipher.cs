﻿/*

    ZyREBOOT - Aplikace restartujici bezpecnostni bran ZyXEL

    Class cipher
    Slouzi k sifrovani a desifrovani dat
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Windows;

namespace ConsoleApplication1
{
    class cipher
    {
         //inicializace klice a init vektoru - musi souhlasit napric aplikacemi
        public static byte[] myRijndaelKey = new byte[32] { 118, 123, 23, 17, 161, 152, 35, 68, 126, 213, 16, 115, 68, 217, 58, 108, 56, 218, 5, 78, 28, 128, 113, 208, 61, 56, 10, 87, 187, 162, 233, 38 };
        public static byte[] myRijndaelIV = new byte[16] { 33, 241, 14, 16, 103, 18, 14, 248, 4, 54, 18, 5, 60, 76, 16, 191 };

        /*
         * HashSHA1 
         * funkce vytvori hash specifickeho souboru
         * @param path cesta k souboru, ze ktereho chce uzivatel vytvorit hash
         */
        public static string HashSHA1(string path)
        {
            string output = "";
            using (FileStream fs = new FileStream(path, FileMode.Open))         

            using (BufferedStream bs = new BufferedStream(fs))
            {
                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    byte[] hash = sha1.ComputeHash(bs);
                    StringBuilder formatted = new StringBuilder(2 * hash.Length);
                    foreach (byte b in hash)
                    {
                        formatted.AppendFormat("{0:X2}", b); //vysledek v hexadecimalnich cislech ve dvojicich
                    }

                    output = formatted.ToString();
                }
            }

            return output;           
        }

        /*
         * EncryptStr 
         * funkce zasifruje text
         * @param plaintext text ktery se ma zasifrovat
         * @param key klic pro sifru
         * @param iv inicializacni vektor pro sifru
         */
        public static String EncryptStr(String plaintext, byte[] key, byte[] iv)
        {
            if (plaintext == null || plaintext.Length <= 0)
            {                
                return null;
            }            

            try
            {
                byte[] ptbytes = Encoding.UTF8.GetBytes(plaintext); //je potreba nacist UTF8 
                MemoryStream ms = new MemoryStream();
                Rijndael rijndael = Rijndael.Create();
                rijndael.Key = key;
                rijndael.IV = iv;

                CryptoStream cs = new CryptoStream(ms, rijndael.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(ptbytes, 0, ptbytes.Length);
                cs.Close();

                byte[] ct = ms.ToArray();

                return Convert.ToBase64String(ct); //Base64 je kodovani dulezite pro zapis do souboru, jinak nelze korektne ulozit
            }
            catch 
            {
                Console.WriteLine("Soubor nemohl být zašifrován.");
                return null;
            }      
        }

        /*
         * DecryptStr 
         * funkce desifruje text
         * @param plaintext text ktery se ma zasifrovat
         * @param key klic pro sifru
         * @param iv inicializacni vektor pro sifru
         */
        public static String DecryptStr(String plaintext, byte[] key, byte[] iv)
        {
            if (plaintext == null || plaintext.Length <= 0)
            {
                return null;
            }

            try
            {
                byte[] ptbytes = Convert.FromBase64String(plaintext); //sifrovany text ulozeny v Base64 kodu - je potreba jej korektne nacist
                MemoryStream ms = new MemoryStream();
                Rijndael rijndael = Rijndael.Create();
                rijndael.Key = key;
                rijndael.IV = iv;

                CryptoStream cs = new CryptoStream(ms, rijndael.CreateDecryptor(), CryptoStreamMode.Write);
                cs.Write(ptbytes, 0, ptbytes.Length);
                cs.Close(); 

                byte[] ct = ms.ToArray();

                return Encoding.UTF8.GetString(ct); //vraceni v UTF8
            }
            catch
            {
                Console.WriteLine("Soubor nemohl být dešifrován, může být poškozena integrita zašifrovaných dat.");
                return null;
            }  

        }
    }
    
}


