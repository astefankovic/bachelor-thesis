﻿/*

    ZyREBOOT - Aplikace restartujici bezpecnostni bran ZyXEL

    Class email
    Slouzi k zasilani informacnich emailu
    
    Copyright (c) Albert Stefankovic, 2015

*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Threading;

namespace ConsoleApplication1
{
    class email
    {

        /*
         * HashSHA1 
         * funkce vodesle email
         * @param subject vyjadruje text v predmetu emailu
         * @param message je obsah odesilane zpravy
         */
        public static void sendEmail(string subject, string message)
        {
            String result;

            try
            {
                //nastaveni parametru pro odeslani e-mailu
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(Program.smtpServer);

                mail.From = new MailAddress(Program.FromEmail);//odesilatel
                mail.To.Add(Program.ToEmail);
                mail.Subject = subject;
                mail.Body = message;

                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = true;
                SmtpServer.Credentials = new System.Net.NetworkCredential(Program.FromEmail, Program.FromEmailPSWD);
                SmtpServer.EnableSsl = true;

                try //osetreni dostupnosti smtp brany
                {
                    SmtpServer.Send(mail); //odeslani e-mailu

                    result = "Byl odeslán e-mail s výsledky.";
                    Console.WriteLine(result);
                    Util.saveToLog(result, "");

                }
                catch                
                {
                    result = "E-mail se nepodařilo odeslat.";
                    Console.WriteLine(result);
                    Util.saveToLog(result, "");
                }

                
                
            }
            catch (Exception ex)
            {
                result =  "Email se nepodařilo odeslat CHYBA:\n" + (ex.ToString());
                Console.WriteLine(result);
                Util.saveToLog(result, "");

                ////ČASOVAČ
                Thread.Sleep(1 * 60 * 1000);
                Environment.Exit(0);
            }

        }


    }
}
