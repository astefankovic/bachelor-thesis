﻿/*

    ZyREBOOT - Aplikace restartujici bezpecnostni bran ZyXEL

    Class getText
    Slouzi k praci s textem
    
    Copyright (c) Albert Stefankovic, 2015
*/

/*
 * Nacteni potrebnych trid a knihoven
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class getText
    {

        public static string[] linesFromFile; //pole ve kterem jsou ulozeny radky ze souboru

        /*
         * readFile 
         * načte text do pole - co řádka v .txt to položka v poli
         */
        public static void readFile(String fileName)
        {
            linesFromFile = System.IO.File.ReadAllLines(Program.path + fileName);
        }

        /*
         * between 
         * parsovani textu - obsah mezi dvema retezci
         */
        public static string between(string value, string a, string b)
        {
            int posA = value.IndexOf(a);
            int posB = value.LastIndexOf(b);
            if (posA == -1)
            {
                return "";
            }
            if (posB == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= posB)
            {
                return "";
            }
            return value.Substring(adjustedPosA, posB - adjustedPosA);
        }

        /*
          * between 
          * parsovani textu - obsah za retezcem
          */
        public static string after(string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }

        /*
         * between 
         * parsovani textu - obsah pred retezcem
         */
        public static string Before(string value, string a)
        {
            int posA = value.IndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            return value.Substring(0, posA);
        }
    }
}
