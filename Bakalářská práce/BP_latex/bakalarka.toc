\select@language {czech}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}V\IeC {\'y}voj po\IeC {\v c}\IeC {\'\i }ta\IeC {\v c}ov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e} v budov\IeC {\v e} gymn\IeC {\'a}zia}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}F\IeC {\'a}ze I. - budov\IeC {\'a}n\IeC {\'\i } dr\IeC {\'a}tov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}F\IeC {\'a}ze II. - budov\IeC {\'a}n\IeC {\'\i } bezdr\IeC {\'a}tov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}F\IeC {\'a}ze III. - nasazen\IeC {\'\i } prvn\IeC {\'\i } bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{3}{section.2.3}
\contentsline {section}{\numberline {2.4}F\IeC {\'a}ze IV. - fin\IeC {\'a}ln\IeC {\'\i } f\IeC {\'a}ze}{4}{section.2.4}
\contentsline {chapter}{\numberline {3}S\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} prvky, technologie a \IeC {\v r}\IeC {\'\i }zen\IeC {\'\i } v~s\IeC {\'\i }ti}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Pasivn\IeC {\'\i } s\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} prvky}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Aktivn\IeC {\'\i } s\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} prvky}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Funkce unifikovan\IeC {\'e} bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{8}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Unifikovan\IeC {\'a} bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}na}{9}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Content filter}{10}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Anti-virus a Anti-spam}{10}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}IDP}{11}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}VPN}{11}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Upozorn\IeC {\v e}n\IeC {\'\i } na probl\IeC {\'e}m v s\IeC {\'\i }ti}{11}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}SSH}{11}{subsection.3.3.7}
\contentsline {section}{\numberline {3.4}Dal\IeC {\v s}\IeC {\'\i } technologie}{12}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Power over Ethernet}{12}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Virtual LAN}{12}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}P\IeC {\v r}eklad s\IeC {\'\i }\IeC {\v t}ov\IeC {\'y}ch adres}{13}{subsection.3.4.3}
\contentsline {section}{\numberline {3.5}Procesy a \IeC {\v r}\IeC {\'\i }zen\IeC {\'\i } v s\IeC {\'\i }ti}{14}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Bezpe\IeC {\v c}nostn\IeC {\'\i } politika s\IeC {\'\i }t\IeC {\v e}}{14}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Bring your own device}{15}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Kryptografie a hashovac\IeC {\'\i } funkce}{15}{section.3.6}
\contentsline {chapter}{\numberline {4}Topologie s\IeC {\'\i }t\IeC {\v e}}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}\IeC {\'U}vod do topologie v budov\IeC {\v e} gymn\IeC {\'a}zia}{16}{section.4.1}
\contentsline {section}{\numberline {4.2}Vytvo\IeC {\v r}en\IeC {\'\i } topologick\IeC {\'e}ho sch\IeC {\'e}matu}{17}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Z\IeC {\'\i }sk\IeC {\'a}n\IeC {\'\i } dat}{18}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}V\IeC {\'y}b\IeC {\v e}r aplikace}{18}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Popis topologie}{19}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Kl\IeC {\'\i }\IeC {\v c}ov\IeC {\'e} prostory}{19}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Servery}{20}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}S\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} prvky}{20}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Pozice a propojen\IeC {\'\i } rack\IeC {\r u}}{21}{subsection.4.3.4}
\contentsline {chapter}{\numberline {5}Anal\IeC {\'y}za po\IeC {\v z}adavk\IeC {\r u}}{22}{chapter.5}
\contentsline {section}{\numberline {5.1}Vlastnosti st\IeC {\'a}vaj\IeC {\'\i }c\IeC {\'\i } unifikovan\IeC {\'e} bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{22}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Hardwarov\IeC {\'a} specifikace sou\IeC {\v c}asn\IeC {\'e} bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{22}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Softwarov\IeC {\'a} specifikace sou\IeC {\v c}asn\IeC {\'e} bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{23}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Po\IeC {\v z}adavky kladen\IeC {\'e} na novou unifikovanou bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}nu}{24}{section.5.2}
\contentsline {section}{\numberline {5.3}Ostatn\IeC {\'\i } po\IeC {\v z}adavky na zm\IeC {\v e}nu po\IeC {\v c}\IeC {\'\i }ta\IeC {\v c}ov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{25}{section.5.3}
\contentsline {section}{\numberline {5.4}Anal\IeC {\'y}za aplikac\IeC {\'\i } komunikuj\IeC {\'\i }c\IeC {\'\i }ch s bezpe\IeC {\v c}nostn\IeC {\'\i } branou}{25}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Aplika\IeC {\v c}n\IeC {\'\i } rozhran\IeC {\'\i }}{26}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Programovac\IeC {\'\i } jazyk}{26}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Form\IeC {\'a}t konfigura\IeC {\v c}n\IeC {\'\i }ch soubor\IeC {\r u}}{27}{subsection.5.4.3}
\contentsline {chapter}{\numberline {6}\IeC {\'U}prava s\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} infrastruktury}{28}{chapter.6}
\contentsline {section}{\numberline {6.1}V\IeC {\'y}padky s\IeC {\'\i }t\IeC {\v e}}{28}{section.6.1}
\contentsline {section}{\numberline {6.2}Fyzick\IeC {\'e} zabezpe\IeC {\v c}en\IeC {\'\i } s\IeC {\'\i }\IeC {\v t}ov\IeC {\'y}ch prvk\IeC {\r u}}{29}{section.6.2}
\contentsline {section}{\numberline {6.3}V\IeC {\'y}m\IeC {\v e}na sou\IeC {\v c}asn\IeC {\'e} unifikovan\IeC {\'e} bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{30}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}V\IeC {\'y}b\IeC {\v e}r vhodn\IeC {\'e}ho za\IeC {\v r}\IeC {\'\i }zen\IeC {\'\i }}{30}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Konfigurace}{33}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}V\IeC {\'y}m\IeC {\v e}na bezdr\IeC {\'a}tov\IeC {\'y}ch access point\IeC {\r u}}{34}{section.6.4}
\contentsline {chapter}{\numberline {7}N\IeC {\'a}vrh aplikac\IeC {\'\i } pro usnadn\IeC {\v e}n\IeC {\'\i } spr\IeC {\'a}vy bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{36}{chapter.7}
\contentsline {section}{\numberline {7.1}Funkcionalita aplikac\IeC {\'\i }}{36}{section.7.1}
\contentsline {section}{\numberline {7.2}Struktura aplikac\IeC {\'\i }}{37}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Knihovny}{38}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Konfigura\IeC {\v c}n\IeC {\'\i } soubory}{39}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Kontroln\IeC {\'\i } soubory}{41}{subsection.7.2.3}
\contentsline {section}{\numberline {7.3}Podpora dal\IeC {\v s}\IeC {\'\i }ch v\IeC {\'y}robc\IeC {\r u}}{41}{section.7.3}
\contentsline {section}{\numberline {7.4}Zaji\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } bezpe\IeC {\v c}nosti aplikac\IeC {\'\i }}{42}{section.7.4}
\contentsline {section}{\numberline {7.5}Po\IeC {\v z}adavky pro b\IeC {\v e}h aplikac\IeC {\'\i }}{42}{section.7.5}
\contentsline {chapter}{\numberline {8}Aplikace ur\IeC {\v c}en\IeC {\'a} pro u\IeC {\v c}itele}{43}{chapter.8}
\contentsline {section}{\numberline {8.1}Popis implementace}{43}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}T\IeC {\v r}\IeC {\'\i }da Cipher}{43}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}T\IeC {\v r}\IeC {\'\i }da Commands}{44}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}T\IeC {\v r}\IeC {\'\i }da FormControl}{45}{subsection.8.1.3}
\contentsline {subsection}{\numberline {8.1.4}T\IeC {\v r}\IeC {\'\i }da GetText}{46}{subsection.8.1.4}
\contentsline {subsection}{\numberline {8.1.5}T\IeC {\v r}\IeC {\'\i }da CheckBoxAndCommand}{46}{subsection.8.1.5}
\contentsline {subsection}{\numberline {8.1.6}T\IeC {\v r}\IeC {\'\i }da MainWindow}{46}{subsection.8.1.6}
\contentsline {subsection}{\numberline {8.1.7}T\IeC {\v r}\IeC {\'\i }da StatusCheck}{46}{subsection.8.1.7}
\contentsline {subsection}{\numberline {8.1.8}T\IeC {\v r}\IeC {\'\i }da Ui}{47}{subsection.8.1.8}
\contentsline {subsection}{\numberline {8.1.9}T\IeC {\v r}\IeC {\'\i }da Util}{47}{subsection.8.1.9}
\contentsline {subsection}{\numberline {8.1.10}T\IeC {\v r}\IeC {\'\i }da WindowContent}{48}{subsection.8.1.10}
\contentsline {section}{\numberline {8.2}Popis u\IeC {\v z}ivatelsk\IeC {\'e}ho prost\IeC {\v r}ed\IeC {\'\i }}{48}{section.8.2}
\contentsline {chapter}{\numberline {9}Aplikace ur\IeC {\v c}en\IeC {\'a} pro administr\IeC {\'a}tora s\IeC {\'\i }t\IeC {\v e}}{50}{chapter.9}
\contentsline {section}{\numberline {9.1}Popis implementace}{50}{section.9.1}
\contentsline {subsection}{\numberline {9.1.1}T\IeC {\v r}\IeC {\'\i }da Cipher}{50}{subsection.9.1.1}
\contentsline {subsection}{\numberline {9.1.2}T\IeC {\v r}\IeC {\'\i }da GetText}{50}{subsection.9.1.2}
\contentsline {subsection}{\numberline {9.1.3}T\IeC {\v r}\IeC {\'\i }da MainWindow}{50}{subsection.9.1.3}
\contentsline {subsection}{\numberline {9.1.4}T\IeC {\v r}\IeC {\'\i }da SetIPWindow}{51}{subsection.9.1.4}
\contentsline {subsection}{\numberline {9.1.5}T\IeC {\v r}\IeC {\'\i }da StatusWindow}{51}{subsection.9.1.5}
\contentsline {subsection}{\numberline {9.1.6}T\IeC {\v r}\IeC {\'\i }da Util}{51}{subsection.9.1.6}
\contentsline {subsection}{\numberline {9.1.7}T\IeC {\v r}\IeC {\'\i }da WindowContent}{52}{subsection.9.1.7}
\contentsline {section}{\numberline {9.2}Popis u\IeC {\v z}ivatelsk\IeC {\'e}ho prost\IeC {\v r}ed\IeC {\'\i }}{52}{section.9.2}
\contentsline {chapter}{\numberline {10}Aplikace pro restart bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{54}{chapter.10}
\contentsline {section}{\numberline {10.1}Popis implementace}{54}{section.10.1}
\contentsline {subsection}{\numberline {10.1.1}T\IeC {\v r}\IeC {\'\i }da Cipher}{54}{subsection.10.1.1}
\contentsline {subsection}{\numberline {10.1.2}T\IeC {\v r}\IeC {\'\i }da Email}{54}{subsection.10.1.2}
\contentsline {subsection}{\numberline {10.1.3}T\IeC {\v r}\IeC {\'\i }da GetText}{55}{subsection.10.1.3}
\contentsline {subsection}{\numberline {10.1.4}T\IeC {\v r}\IeC {\'\i }da Program}{55}{subsection.10.1.4}
\contentsline {subsection}{\numberline {10.1.5}T\IeC {\v r}\IeC {\'\i }da Util}{55}{subsection.10.1.5}
\contentsline {section}{\numberline {10.2}Popis u\IeC {\v z}ivatelsk\IeC {\'e}ho prost\IeC {\v r}ed\IeC {\'\i }}{55}{section.10.2}
\contentsline {chapter}{\numberline {11}Testov\IeC {\'a}n\IeC {\'\i }}{56}{chapter.11}
\contentsline {section}{\numberline {11.1}Testov\IeC {\'a}n\IeC {\'\i } s\IeC {\'\i }t\IeC {\v e} vzhledem k proveden\IeC {\'y}m zm\IeC {\v e}n\IeC {\'a}m}{56}{section.11.1}
\contentsline {subsection}{\numberline {11.1.1}Test 1 - ov\IeC {\v e}\IeC {\v r}en\IeC {\'\i } zabezpe\IeC {\v c}en\IeC {\'\i } wifi}{56}{subsection.11.1.1}
\contentsline {subsection}{\numberline {11.1.2}Test 2 - Propustnost s\IeC {\'\i }t\IeC {\v e}}{57}{subsection.11.1.2}
\contentsline {section}{\numberline {11.2}Testov\IeC {\'a}n\IeC {\'\i } aplikac\IeC {\'\i }}{57}{section.11.2}
\contentsline {subsection}{\numberline {11.2.1}Test 1 - nedostupnost bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{58}{subsection.11.2.1}
\contentsline {subsection}{\numberline {11.2.2}Test 2 - chyb\IeC {\v e}j\IeC {\'\i }c\IeC {\'\i } konfigura\IeC {\v c}n\IeC {\'\i } soubory}{59}{subsection.11.2.2}
\contentsline {subsection}{\numberline {11.2.3}Test 3 - pokus o p\IeC {\v r}eps\IeC {\'a}n\IeC {\'\i } konfigura\IeC {\v c}n\IeC {\'\i }ho souboru}{60}{subsection.11.2.3}
\contentsline {subsection}{\numberline {11.2.4}Test 4 - pokus o zablokov\IeC {\'a}n\IeC {\'\i } konkr\IeC {\'e}tn\IeC {\'\i } str\IeC {\'a}nky v konkr\IeC {\'e}tn\IeC {\'\i } t\IeC {\v r}\IeC {\'\i }d\IeC {\v e}}{60}{subsection.11.2.4}
\contentsline {section}{\numberline {11.3}Vyhodnocen\IeC {\'\i } test\IeC {\r u}}{61}{section.11.3}
\contentsline {chapter}{\numberline {12}Zhodnocen\IeC {\'\i } p\IeC {\v r}\IeC {\'\i }nosu realizovan\IeC {\'y}ch vylep\IeC {\v s}en\IeC {\'\i }}{62}{chapter.12}
\contentsline {section}{\numberline {12.1}P\IeC {\v r}\IeC {\'\i }nos zm\IeC {\v e}n v topologii s\IeC {\'\i }t\IeC {\v e}}{62}{section.12.1}
\contentsline {section}{\numberline {12.2}P\IeC {\v r}\IeC {\'\i }nos aplikac\IeC {\'\i }}{63}{section.12.2}
\contentsline {section}{\numberline {12.3}Mo\IeC {\v z}n\IeC {\'a} roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i }}{63}{section.12.3}
\contentsline {chapter}{\numberline {13}Z\IeC {\'a}v\IeC {\v e}r}{64}{chapter.13}
\contentsline {chapter}{Literatura}{66}{chapter*.43}
