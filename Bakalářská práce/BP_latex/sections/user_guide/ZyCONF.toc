\select@language {czech}
\contentsline {section}{\numberline {1}Popis aplikace}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Funkce aplikace}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}T\IeC {\v r}\IeC {\'\i }dy u\IeC {\v z}ivatel\IeC {\r u}}{1}{subsection.1.2}
\contentsline {section}{\numberline {2}Instalace a spu\IeC {\v s}t\IeC {\v e}n\IeC {\'\i }}{2}{section.2}
\contentsline {section}{\numberline {3}Ovl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } aplikace}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}P\IeC {\v r}ihl\IeC {\'a}\IeC {\v s}en\IeC {\'\i } do aplikace}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Hlavn\IeC {\'\i } okno aplikace}{2}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Z\IeC {\'\i }sk\IeC {\'a}n\IeC {\'\i } stavu bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{3}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Spustit opakovan\IeC {\v e}}{4}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Nastaven\IeC {\'\i } ov\IeC {\v e}\IeC {\v r}ovan\IeC {\'y}ch IP adres}{5}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Vytvo\IeC {\v r}en\IeC {\'\i } konfigura\IeC {\v c}n\IeC {\'\i }ho souboru connection.txt}{5}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Logov\IeC {\'a}n\IeC {\'\i }}{8}{subsection.3.7}
\contentsline {section}{\numberline {4}Popis konfigura\IeC {\v c}n\IeC {\'\i }ch soubor\IeC {\r u}}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Obsah souboru \textit {connection.txt}}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Obsah souboru \textit {ui.txt}}{10}{subsection.4.2}
\contentsline {section}{\numberline {5}Nastaven\IeC {\'\i } pravidel bezpe\IeC {\v c}nostn\IeC {\'\i } br\IeC {\'a}ny}{11}{section.5}
\contentsline {section}{\numberline {6}Chybov\IeC {\'e} hl\IeC {\'a}\IeC {\v s}ky}{12}{section.6}
