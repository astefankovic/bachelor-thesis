
Cílem této práce je modernizace a rozšíření správy školní počítačové sítě. Za úkol si klade především vyhovění požadavkům vedení Gymnázia v Prachaticích pro zlepšení jejich počítačové sítě a vytvoření aplikace pro pomoc se správou a konfigurací sítě s ohledem na jednoduché používání. Práce byla tvořena ve spolupráci s firmou Tribase, s. r. o., která je externím zadavatelem tématu této práce.

Gymnázium prochází postupnou změnou IT infrastruktury. Tato práce zastřešuje finální fázi, ve které jde o nasazení zařízení pro bezpečnost sítě (firewall, unifikovaná bezpečnostní brána) a dalších síťových zařízení, které by měli zvětšit propustnost sítě, zvýšit její bezpečnost a usnadnit její správu. V průběhu návrhu této fáze byla s vedením gymnázia diskutována možnost nasazení aplikace, která by byla schopna ovládat tok datového obsahu v počítačových učebnách. Spolu s ní bude vytvořena i aplikace pro administrátora sítě, jejíž hlavní funkcí bude zjednodušení práce s konfigurací a údržbou sítě.

Aplikace pro správu datového obsahu v počítačových učebnách bude určena především pro učitele, kteří budou moci jednoduše zablokovat různé druhy webových stránek, nebo veškerý síťový provoz učebny. Aplikace pro administrátora sítě bude plně oddělena od učitelské aplikace. Aplikace bude administrátorovi poskytovat základní informace o stavu unifikované bezpečnostní brány a bude plnit úkony zaměřené na ulehčení správy sítě.

Obě aplikace budou naprogramovány pro operační systém Microsoft Windows v programovacím jazyce C#, na platformě Microsoft .NET. Aplikace budou komunikovat s unifikovanými bezpečnostními branami ZyXEL a ovládání bude v českém jazyce. U každé aplikace bude kladen důraz na jednoduchost a přívětivost uživatelského rozhraní.

Závěr práce bude věnován zhodnocení realizovaných vylepšení, ověření funkcionality provedených změn a otestování vytvořených aplikací.

\Aplikace

	ZyCONFIG.zip

	ZyREBOOT.zip

	ZyCONTENT.zip

	\Uživatelské manuály

	\Zdrojové soubory

	readme.txt

\Bakalářská práce

	bakalarska_prace_Albert_Stefankovic.pdf

	BP_latech


\Topologie sítě

	\dia_zdroj

	\pdf

\Ostatní

	ZyXEL_USG110_CLI.pdf

	ZyXEL_USG110_UserManual.pdf

readme.txt